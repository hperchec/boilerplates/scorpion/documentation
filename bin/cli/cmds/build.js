const fs = require('fs-extra')
const path = require('path')
const juisy = require('@hperchec/juisy')

// Utils
const {
  rootDir,
  $style,
  log,
  run,
  step,
  substep,
  error,
  wait
} = juisy.utils

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'build <targetDir>',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Build documentation',
  /**
   * Builder
   * @param {Object} yargs
   * @return {Object}
   */
  builder: function (yargs) {
    return yargs
  },
  /**
   * Handler
   * @param {Object} argv - The argv
   * @return {void}
   */
  handler: async function (argv) {
    const defaultOutputPath = path.resolve(rootDir, argv.targetDir, '.vuepress/dist')
    const finalOutputPath = path.resolve(rootDir, 'dist', argv.targetDir)
    /**
     * Build
     */
    step('Build')
    // ⚠ IMPORTANT: default output dir is .vuepress/dist folder
    // THERE IS A BUG with custom "dest" option path in .vuepress/config.js file
    // => vuepress build command will fail if the path contains dots "." ...
    // So, after output in default folder, we move it into ./dist/<targetDir> folder (relative from root)
    await wait('Compiling', async () => {
      try {
        await run(
          'vuepress',
          [
            'build',
            argv.targetDir
          ],
          { stdio: 'pipe' }
        )
      } catch (e) {
        error('Unable to build documentation', e)
      }
    })
    substep($style.green('✔ Success'), { last: true })
    log() // Blank line
    /**
     * Move destination folder
     */
    step('Finalize')
    await wait('Copying files', async () => {
      try {
        fs.moveSync(defaultOutputPath, finalOutputPath, { overwrite: true })
      } catch (e) {
        error(`Unable to move files in destination folder "${finalOutputPath}"`, e)
      }
    })
    substep($style.green('✔ Success'), { last: true })
    log() // Blank line
  }
}
