const path = require('path')
const juisy = require('@hperchec/juisy')
const { checkMd, getContent } = require('../lib/check-md/')
const { compose, deeplyParseHeaders, slugify } = require('@vuepress/shared-utils')

// Utils
const {
  $style,
  log,
  step,
  substep,
  error,
  wait
} = juisy.utils

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'check-md <targetDir>',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Check dead links in markdown files',
  /**
   * Builder
   * @param {Object} yargs
   * @return {Object}
   */
  builder: function (yargs) {
    return yargs
  },
  /**
   * Handler
   * @param {Object} argv - The argv
   * @return {void}
   */
  handler: async function (argv) {
    /**
     * Check md
     */
    step('Check markdown links')
    await wait('Checking', async () => {
      success = await checkMd({
        // Target directory
        targetDir: argv.targetDir,
        // Slugify method: same as vuepress-check-md plugin
        // See also: https://github.com/ulivz/vuepress-plugin-check-md/blob/a8601505b7e1e99f45f517727549019c51dcf377/index.js#L23
        slugify: compose(deeplyParseHeaders, slugify),
        // Do something with file object before caching
        onReadFile: (fileUrl, fileObj) => {
          // Get named anchors added by jsdoc2md (dmd) template
          const namedAnchors = []
          // Deep parse token to find anchors
          function deepParseToken (_token) {
            if (_token.type === 'html') {
              const anchorRE = /^<a name="(.*)">$/g
              const matches = [ ..._token.raw.matchAll(anchorRE) ]
              const isAnchor = Boolean(matches[0])
              if (isAnchor) namedAnchors.push(matches[0][1])
            }
            if (_token.tokens) {
              for (const t of _token.tokens) {
                deepParseToken(t)
              }
            }
          }
          for (const token of fileObj.content.tokens) {
            deepParseToken(token)
          }
          // Set namedAnchors property for cache object
          fileObj.content.namedAnchors = namedAnchors
        },
        // Do something with each file once checked and cached
        forEachCachedFile: async (fileUrl, fileObj) => {
          /**
           * We need to parse file to find named anchors (<a name="xxxx"></a>) added by jsdoc2md template (dmd)
           * If an error of type "Hash is not found" occurs (from check-md lib result), check if this anchor name exist.
           * In that case, simply ignore this error.
           */
          const indexesToRemove = []
          // Loop on dead links of type "Hash is not found"
          for (const index in fileObj.errors.deadLinks.hashNotFound) {
            const deadlink = fileObj.errors.deadLinks.hashNotFound[index]
            // Get anchor name from deadlink.matchUrl
            const anchorName = [ ...deadlink.original.matchUrl.matchAll(/#(.*)$/g) ][0][1]
            const isUrlRelative = !(deadlink.original.matchUrl.startsWith('#'))
            // If relative url, find file
            if (isUrlRelative) {
              // Get path without hash and anchor
              let targetPath = deadlink.original.matchUrl.replace(/#(.*)$/, '') // remove hash
              const isDir = targetPath.endsWith('/')
              targetPath = path.resolve(path.dirname(fileUrl), targetPath) // resolve
              if (isDir) {
                targetPath = path.join(targetPath, 'README.md')
              } else {
                targetPath += targetPath.endsWith('.md') ? '' : '.md'
              }
              // Get cached file obj
              const targetFileObj = await getContent(targetPath)
              // Remove error if named anchor found
              if (targetFileObj.content.namedAnchors.includes(anchorName)) {
                // log($style.yellow(`ignore EXTERNAL anchor name: "${anchorName}" in file: "${targetPath}"`))
                indexesToRemove.push(Number(index))
              }
            } else {
              // Else, find named anchor in this file
              if (fileObj.content.namedAnchors.includes(anchorName)) {
                // log($style.yellow(`ignore IN FILE anchor name: "${anchorName}"`))
                indexesToRemove.push(Number(index))
              }
            }
          }
          // Remove 'hashNotFound' typed errors that must be ignored
          fileObj.errors.deadLinks.hashNotFound = fileObj.errors.deadLinks.hashNotFound.reduce((include, errObj, index) => {
            if (!(indexesToRemove.includes(index))) include.push(errObj)
            return include
          }, [])
        },
        // On success
        onSuccess: () => {
          substep($style.green('✔ Success'), { last: true })
          log() // Blank line
        },
        // On fail
        onFail: (filesInError) => {
          substep($style.red('❌ Failed'), { last: true })
          log() // Blank line
          // Loop on files in error
          for (let [fileUrl, fileObj] of filesInError) {
            // Check if file has error
            log($style.yellow.bold.underline(`File: ${fileUrl}`))
            // Warnings
            const warnings = Object.keys(fileObj.warnings).reduce((array, type) => {
              for (const warn of fileObj.warnings[type]) array.push(warn)
              return array
            }, [])
            if (warnings.length > 0) {
              log($style.yellow.bold(`Warnings:`), { indent: 2 })
              for (const warn of warnings) log('- ' + warn.styledMessage, { indent: 4 })
            }
            // Dead links
            const deadLinks = Object.keys(fileObj.errors.deadLinks).reduce((array, type) => {
              for (const error of fileObj.errors.deadLinks[type]) array.push(error)
              return array
            }, [])
            if (deadLinks.length > 0) {
              log($style.red.bold(`Link errors:`), { indent: 2 })
              for (const error of deadLinks) log('- ' + error.styledMessage, { indent: 4 })
            }
            // Lint errors
            const lintErrors = fileObj.errors.lint
            if (lintErrors.length > 0) {
              log($style.red.bold(`Lint:`), { indent: 2 })
              for (const error of lintErrors) log('- ' + error.styledMessage, { indent: 4 })
            }
            log() // Blank Line
          }
          error('Checking failed')
        }
      })
    })
  }
}
