const fs = require('fs')
const path = require('path')
const juisy = require('@hperchec/juisy')
const marked = require('marked')
const markdownlint = require('markdownlint')
const { check } = require('check-md')
const glob = require('globby')

// Utils
const {
  $style,
  log,
  error,
  rootDir
} = juisy.utils

/**
 * @typedef {Object} CacheObj
 * @property {String} CacheObj.fileUrl - The file url
 * @property {Object} CacheObj.content
 * @property {Array<Object>} CacheObj.content.tokens - The tokens from marked.lexer result
 * @property {String} CacheObj.content.raw - The original file content (raw format)
 * @property {Array<Object>} CacheObj.warnings - The warnings object
 * @property {Array<Object>} CacheObj.deadLinks - The dead links object
 */

/**
 * Hook class
 */
class Hook {
  constructor () {
    // ....
  }
  /**
   * @type {Function[]}
   * @private
   */
  _queue = []
  /**
   * Add a function to exec to the queue
   * @param {Function|Function[]} func - The function to append to the hook queue. Can be an array of functions.
   * @return {void}
   */
  append (func) {
    const pushToQueue = (f) => {
      if (typeof f !== 'function') {
        throw new Error('Hook class: the item to append to the queue must be Function type.')
      }
      // Push new function to queue
      this._queue.push(f)
    }
    if (func) {
      if (func instanceof Array) {
        for (const _func of func) {
          pushToQueue(_func)
        }
      } else {
        pushToQueue(func)
      }
    }
  }
  /**
   * exec
   * @param {...*} args - The arguments to pass to each queue function
   * @return {Promise}
   * @fulfil void
   * @description
   * Execute the queue asynchronously (in the order of execution)
   */
  async exec (...args) {
    for (const func of this._queue) {
      await func(...args)
    }
  }
}

/**
 * @type {Map<String, CacheObj>}
 */
const cache = new Map()

/**
 * check-md error types
 * @type {Object}
 * @property {Object} warning
 * @property {String} warning.shouldUseDotMd
 * @property {Object} error
 * @property {String} error.urlLinkEmpty
 * @property {String} error.fileNotFound
 * @property {String} error.hashShouldSlugify
 * @property {String} error.hashNotFound
 */
const checkMdErrorTypes = {
  warning: {
    shouldUseDotMd: 'Should use .md instead of .html'
  },
  error: {
    urlLinkEmpty: 'Url link is empty',
    fileNotFound: 'File is not found',
    hashShouldSlugify: 'Hash should slugify',
    hashNotFound: 'Hash is not found'
  }
}

const onReadFile = new Hook()
const forEachCachedFile = new Hook()
const onSuccess = new Hook()
const onFail = new Hook()

exports.cache = cache
exports.checkMdErrorTypes = checkMdErrorTypes
exports.getCheckMdErrorTypeFromMsg = getCheckMdErrorTypeFromMsg
exports.getFileUrls = getFileUrls
exports.getContent = getContent
exports.getFilesInError = getFilesInError
exports.checkMd = checkMd

/**
 * Get errorType based on error message returned by check method (from check-md lib)
 * @param {string} level - The error level ("error", "warning")
 * @param {string} message - The error message (ex: "File is not found")
 * @return {string}
 */
function getCheckMdErrorTypeFromMsg (level, message) {
  let found
  const types = checkMdErrorTypes[level]
  for (const type in types) {
    if (types[type] === message) found = type
  }
  if (!found) error(`Can't find error type for error message: "${message}"`)
  else return found
}

/**
 * Get file URLs to parse / check
 * @param {String} targetDir - The target directory
 * @param {String|String[]} pattern - The pattern to match
 * @param {String|String[]} ignore - The pattern to ignore
 * @param {String[]} root - The root directories
 * @return {String}
 */
async function getFileUrls (targetDir, pattern, ignore, root) {
  if (!(Array.isArray(root))) error('options.root can be an array of string')
  const _pattern = []
  for (const p of (Array.isArray(pattern) ? pattern : [ pattern ])) {
    for (const r of root) {
      _pattern.push(path.posix.join(r, p))
    }
  }
  const _ignore = Array.isArray(ignore) ? ignore : [ ignore ]
  const globPattern = _pattern.concat(_ignore.map(p => `!${p}`))
  const files = await glob(globPattern, { cwd: targetDir })
  return files.map(p => path.resolve(rootDir, targetDir, p))
}

/**
 * get content with cache
 * @param {String} fileUrl - fileUrl
 * @return {CacheObj} - CacheObj
 */
async function getContent(fileUrl) {
  if (cache.has(fileUrl)) {
    return cache.get(fileUrl)
  }
  // Read file content
  const content = fs.readFileSync(fileUrl, { encoding: 'utf-8' })
  // Get markdown tokens with marked.lexer
  const tokens = marked.lexer(content)
  // Object
  const cacheObj = {
    fileUrl: fileUrl,
    content: {
      tokens: tokens,
      raw: content
    },
    warnings: Object.keys(checkMdErrorTypes.warning).reduce((obj, value) => {
      obj[value] = []
      return obj
    }, {}),
    errors: {
      deadLinks: Object.keys(checkMdErrorTypes.error).reduce((obj, value) => {
        obj[value] = []
        return obj
      }, {}),
      lint: [] // Empty array by default
    }
  }
  await onReadFile.exec(fileUrl, cacheObj)
  cache.set(fileUrl, cacheObj)
  return cacheObj
}

/**
 * Returns files in error (or warning)
 * @return {Map<String, CacheObj>}
 */
function getFilesInError () {
  const files = new Map()
  for (let [fileUrl, cacheObj] of cache) {
    let length = 0
    // Get warnings length
    for (const type in cacheObj.warnings) length += cacheObj.warnings[type].length
    // Get dead links length
    for (const type in cacheObj.errors.deadLinks) length += cacheObj.errors.deadLinks[type].length
    // Get lint errors length
    length += cacheObj.errors.lint.length
    // Push if errors or warnings
    if (length > 0) {
      files.set(fileUrl, cacheObj)
    }
  }
  return files
}

/**
 * Parse check-md method result
 * @param {Object} - The result of checkmd method
 * @return {void}
 */
async function parseCheckMdResult (checkMdResult) {
  // Loop on warnings
  for (const warning of checkMdResult.warning.list) {
    // Get warning type
    const warnType = getCheckMdErrorTypeFromMsg('warning', warning.errMsg)
    // Get cached file object
    const fileObj = await getContent(warning.fileUrl)
    // Build message & styled message
    const message = `${warning.errMsg}: ${warning.fullText} (line: ${warning.line}, col: ${warning.col})`
    const styledMessage = `${$style.bold(warning.errMsg)}: ${warning.fullText} ${$style.italic.grey(`(line: ${warning.line}, col: ${warning.col})`)}`
    // Push warning
    fileObj.warnings[warnType].push({
      line: warning.line,
      column: warning.col,
      message: message,
      styledMessage: styledMessage,
      original: warning
    })
  }
  // Loop on dead links
  for (const deadlink of checkMdResult.deadlink.list) {
    // Get error type
    const errorType = getCheckMdErrorTypeFromMsg('error', deadlink.errMsg)
    // Get cached file object
    const fileObj = await getContent(deadlink.fileUrl)
    // Build message & styled message
    const message = `${deadlink.errMsg}: ${deadlink.fullText} (line: ${deadlink.line}, col: ${deadlink.col})`
    const styledMessage = `${$style.bold(deadlink.errMsg)}: ${deadlink.fullText} ${$style.italic.grey(`(line: ${deadlink.line}, col: ${deadlink.col})`)}`
    // Push error
    fileObj.errors.deadLinks[errorType].push({
      line: deadlink.line,
      column: deadlink.col,
      message: message,
      styledMessage: styledMessage,
      original: deadlink
    })
  }
}

/**
 * Parse markdownlint result
 * @param {LintResults} markdownlintResult - The markdownlint results object
 * @return {void}
 */
async function parseMarkdownlintResult (markdownlintResult) {
  for (const fileUrl in markdownlintResult) {
    // Get lint errors
    const lintErrors = markdownlintResult[fileUrl]
    // Get cached file object
    const fileObj = await getContent(fileUrl)
    // Loop on lint errors
    for (const lintError of lintErrors) {
      const message = `[${lintError.ruleNames[0]}] ${lintError.ruleDescription}${lintError.errorContext ? `: ${lintError.errorContext}` : ''} (line: ${lintError.lineNumber})`
      const styledMessage = `${$style.bold(`[${lintError.ruleNames[0]}] ${lintError.ruleDescription}`)}${lintError.errorContext ? `: ${lintError.errorContext}` : ''} ${$style.italic.grey(`(line: ${lintError.lineNumber})`)}${lintError.ruleInformation ? $style.grey(` (see: ${lintError.ruleInformation})`) : ''}`
      // Push error
      fileObj.errors.lint.push({
        line: lintError.lineNumber,
        column: undefined,
        message: message,
        styledMessage: styledMessage,
        original: lintError
      })
    }
    // console.log('result lmint : ', markdownlintResult[file])
  }
  // console.log(markdownlintResult.toString())
}

/**
 * Check markdown dead links (using check-md lib)
 * @param {Object} options 
 * @param {string} options.targetDir - The target directory (same as check-md: CheckOption.cwd)
 * @param {string} [options.lintConfig] - The markdownlint config file relative path
 * @param {Array<String>} [options.root = ['./', './.vuepress/public']] - Root directory(ies) (same as check-md: CheckOption.root)
 * @param {Array<String>} [options.defaultIndex = ['README.md', 'index.md']] - Default index(es) file(s) (same as check-md: CheckOption.defaultIndex)
 * @param {String | Array<String>} [options.pattern = '**\/*.md'] - Pattern (same as check-md: CheckOption.pattern)
 * @param {String | Array<String>} [options.ignore = ['**\/node_modules']] - Files to ignore (same as check-md: CheckOption.ignore)
 * @param {Boolean} [options.ignoreFootnotes = false] - Ignore foot notes (same as check-md: CheckOption.ignoreFootnotes)
 * @param {Function} [options.slugify] - Slugify option (same as check-md: CheckOption.slugify)
 * @param {Function | Array<Function>} [options.onReadFile] - Callback function to call when file is readen before caching. Can be an array of functions. Function signature is `function (fileUrl, cacheObj): CacheObj | void | Promise<CacheObj|void>`
 * @param {Function | Array<Function>} [options.forEachCachedFile] - Callback function to call when files are cached. Can be an array of functions. Function signature is `function (fileUrl, cacheObj): void | Promise<void>`
 * @param {Function | Array<Function>} [options.onSuccess] - Callback function if success. Can be an array of functions. Function signature is `function (): void | Promise<void>`
 * @param {Function | Array<Function>} [options.onFail] - Callback function if fail. Can be an array of functions. Function signature is `function (error): void | Promise<void>`
 * @return {boolean}
 */
async function checkMd (options) {
  // Process global options
  if (!(options.targetDir)) error('checkMd: options.targetDir is nod readable')
  const targetDir = options.targetDir
  const root = options.root || ['./', './.vuepress/public']
  const defaultIndex = options.defaultIndex || [ 'README.md', 'index.md' ]
  const pattern = options.pattern || '**/*.md'
  const ignore = options.ignore || [ '**/node_modules' ]
  // Get markdownlint config
  let markdownLintConfig = {} // empty object by default
  if (options.lintConfig) { // if option is provided
    const configPath = path.resolve(rootDir, options.lintConfig)
    // Try to get config from file
    markdownLintConfig = JSON.parse(fs.readFileSync(configPath, { encoding: 'utf8' }))
  } else {
    // Try to get .markdownlint.json file
    const configPath = path.resolve(rootDir, '.markdownlint.json')
    if (fs.existsSync(configPath)) {
      markdownLintConfig = JSON.parse(fs.readFileSync(configPath, { encoding: 'utf8' }))
    }
  }
  // Get file urls
  const fileUrls = await getFileUrls(targetDir, pattern, ignore, root)
  // Process hook type options
  onReadFile.append(options.onReadFile)
  forEachCachedFile.append(options.forEachCachedFile)
  onSuccess.append(options.onSuccess)
  onFail.append(options.onFail)
  // Call check-md 'check' method
  const checkMdResult = await check({
    pattern: pattern,
    ignore: ignore,
    exitLevel: 'error',
    root: root,
    defaultIndex: defaultIndex,
    ...( options.slugify ? { slugify: options.slugify } : {}),
    cwd: options.targetDir
  })
  // Parse check-md result
  await parseCheckMdResult(checkMdResult)
  // Call markdownlint 'sync' method
  const markdownlintResult = markdownlint.sync({
    files: fileUrls,
    config: markdownLintConfig
  })
  // Parse markdownlint result
  await parseMarkdownlintResult(markdownlintResult)
  // Main loop on cached files
  for (let [fileUrl, fileObj] of cache) {
    // Call hook
    await forEachCachedFile.exec(fileUrl, fileObj)
  }
  const checkPassed = getFilesInError().size === 0
  if (!checkPassed) {
    // Call hook
    await onFail.exec(getFilesInError())
    return false
  } else {
    // Call hook
    await onSuccess.exec()
    return true
  }
}