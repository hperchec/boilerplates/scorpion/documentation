const apiSidebar = require('./api/sidebar.js')

module.exports = {
  // Sidebar
  '/': [
    {
      title: 'Guide',
      path: '/guide/',
      collapsable: false,
      children: [
        // Get started
        {
          title: 'Get started',
          path: '/guide/get-started/'
        },
        // Directory structure
        {
          title: 'Directory structure',
          path: '/guide/directory-structure/'
        },
        // Internal flow
        {
          title: 'Internal flow',
          path: '/guide/internal-flow/'
        },
        // Configuration
        {
          title: 'Configuration',
          path: '/guide/configuration/'
        },
        // Globals
        {
          title: 'Globals',
          path: '/guide/globals/'
        },
        // Built-in Services
        {
          title: 'Services',
          path: '/guide/services/',
          collapsable: true,
          children: [
            // api-manager
            {
              title: 'API manager',
              path: '/guide/services/api-manager/'
            },
            // event-bus
            {
              title: 'Event bus',
              path: '/guide/services/event-bus/'
            },
            // i18n
            {
              title: 'Internationalization',
              path: '/guide/services/i18n/'
            },
            // local-storage-manager
            {
              title: 'Local storage',
              path: '/guide/services/local-storage-manager/'
            },
            // logger
            {
              title: 'Logger',
              path: '/guide/services/logger/'
            },
            // modal-manager
            {
              title: 'Modals',
              path: '/guide/services/modal-manager/'
            },
            // router
            {
              title: 'Router',
              path: '/guide/services/router/'
            },
            // store
            {
              title: 'Store',
              path: '/guide/services/store/'
            },
            // toast-manager
            {
              title: 'Toasts',
              path: '/guide/services/toast-manager/'
            }
          ]
        },
        // CLI
        {
          title: 'CLI',
          path: '/guide/cli/'
        },
        // Dev tools
        {
          title: 'Dev tools',
          path: '/guide/dev-tools/'
        },
        // Build
        {
          title: 'Build',
          path: '/guide/build/'
        }
      ]
    },
    // Cookbook
    {
      title: 'Cookbook',
      path: '/cookbook/',
      collapsable: false,
      children: []
    },
    // API
    {
      title: 'API',
      path: '/api/',
      collapsable: false,
      children: apiSidebar
    }
  ]
}
