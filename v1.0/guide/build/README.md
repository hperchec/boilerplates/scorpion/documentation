---
sidebarDepth: 1
---

# Build

There are two tools used for building: **webpack** and **cordova**.

Webpack compilation will bundle our "web-ready" source code, while Cordova
will do each platform specific build based on the previously created webpack bundle.

## Webpack

As a standard Vue project, you can configure via `vue.config.js` file at the root.
See also [Vue configuration](https://cli.vuejs.org/config/) documentation.

::: tip Note
Note that `createConfig` tool is used in the default `vue.config.js` file.

This method is imported from `@hperchec/scorpion-ui/shared/vue/config/create-config`
and takes the following parameters:

- `{Object} globals`: The globals object
- `{String} rootPath`: The project root path
- `{Object} [config = {}]`: The custom configuration to merge
- `{Object} [options = {}]`: The options object
- `{String} [options.outputDir = 'www']`: Output path
  (relative to root directory)
- `{String} [options.jsOutputPath = 'js']`: JS output subdirectory
  (relative to output directory)
- `{String} [options.cssOutputPath = 'css']`: CSS output subdirectory
  (relative to output directory).

Result is a merge of provided objects.
:::

### Aliases

The following [webpack aliases](https://webpack.js.org/configuration/resolve/#resolvealias)
are defined by default:

|Alias            |Path                  |
|-                |-                     |
|`~`              |`./`                  |
|`@`              |`./src`               |
|`#components`    |`./src/components`    |
|`#config`        |`./src/config`        |
|`#i18n`          |`./src/i18n`          |
|`#mixins`        |`./src/mixins`        |
|`#models`        |`./src/models`        |
|`#store`         |`./src/store`         |
|`#styles`        |`./src/scss`          |

You can define your owns in `configureWebpack.resolve.alias`.

## Cordova

::: WIP
Work in progress...
:::

## Generate documentation

::: WIP
Work in progress...
:::

### API doc

WIP

### README file

See also @hperchec/readme-generator doc and juisy doc