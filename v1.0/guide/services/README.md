# Services

Scorpion UI provides the following built-in services:

- [api-manager](/guide/api/services/api-manager/)
- [event-bus](/guide/api/services/event-bus/)
- [i18n](/guide/api/services/i18n/)
- [local-storage-manager](/guide/api/services/local-storage-manager/)
- [logger](/guide/api/services/logger/)
- [modal-manager](/guide/api/services/modal-manager/)
- [router](/guide/api/services/router/)
- [store](/guide/api/services/store/)
- [toast-manager](/guide/api/services/toast-manager/)

## How services work

Services are objects that add features to our app. This is the ScorpionUI's way
to easily achieve this goal. A service can be a [Vue plugin](https://v2.vuejs.org/v2/guide/plugins.html#ad),
see below how to create a service. Else, use the appropriate tools to create
your service object and add your own features.

Service must have an `identifier` **{String}** in kebabcase format (example: `my-service`).

Services automatically have a dedicated namespace in global configuration under
the property named by service `identifier`.
For example, the service `my-service` will have configuration namespace in `Core.config.services['my-service']`.
What you put in this configuration object depends on your own logic.
Also, properties in configuration object can be related to service options object
properties, see below for examples.

Services are registered in `Core.context.services` before creation.
Built-in services are automatically registered when calling the `Core.init()` method.
Use the `Core.registerService()` method to add your own service.

When `Core.setup()` method is called, it will loop on each registered service
to call the **BEFORE_CREATE_SERVICE** hook, the `create()` service method
and the **SERVICE_CREATED** hook.

Once services are created, so they are instanciated and accessible via `Core.service(<identifier>)`.

Let's have a look to this schema to understand how services are registered and created:

<Mermaid filePath="/guide/services/_mermaid/services-lifecycle.mmd"/>

## Create a service

Each service should follow the same structure:

- `index.js`: the service definition file that exports the [Service](/guide/api/services/Service) instance
- `options.js`: the service options file that exports the [options](/guide/api/services/Service#options) object
- `MyService.js` *(example)*: optional, the service class file

### Definition

In the `index.js` file, export a new instance of [Service](/guide/api/services/Service) class:

```js
import { Core, Service } from '@hperchec/scorpion-ui'
import MyService from './MyService'
import options from './options'

export default new Service({
  /**
   * The service definition object
   */
  service: {
    /**
     * Service identifier
     */
    identifier: 'my-service',
    /**
     * The register callback: this function will be called on service registering.
     * See services hooks documentation.
     * @type {Function}
     * @param {Function} _Core - The Core class
     * @returns {void}
     */
    register: function (_Core) {
      // ...
    },
    /**
     * The create service method. Returns the service instance / object
     * that will be accessible via Core.service('<identifier>').
     * @type {Function}
     * @param {Object} options - The service options object
     * @param {Object} serviceContext - The service exposed namespace (`Core.context.services['<identifier>']`)
     * @param {Function} _Core - The Core class
     * @returns {any} - The service object / instance to return when calling `Core.service('<identifier>')`
     */
    create: (options, serviceContext, _Core) => {
      // Return new MyService instance
      return new serviceContext.MyService(options)
    },
    /**
     * Service can register vue plugin.
     * By passing this option, the returned vue plugin will be automatically injected
     * @type {Function}
     * @param {Object} serviceContext - The service exposed namespace
     * @param {Function} _Core - The Core class
     * @returns {Function|Object} - The object / class that represents the Vue plugin injected by our service
     */
    vuePlugin: (serviceContext, _Core) => serviceContext.MyService,
    /**
     * Vue plugin options
     * @type {Object}
     */
    pluginOptions: {},
    /**
     * To expose the plugin instance in Vue root instance option. Can be String or Function that returns string.
     * Example: service "store" will expose Vuex.Store instance to root instance "$store" option.
     * @type {String|Function}
     * @param {Object} serviceContext - The service exposed namespace
     * @param {Function} _Core - The Core class
     * @returns {String}
     */
    rootOption: (serviceContext, _Core) => serviceContext.MyService.rootOption
  },
  /**
   * Service options object
   */
  options,
  /**
   * The namespaced object that will be exposed in Core.context.services['my-service']
   */
  expose: {
    /**
     * Expose service class
     */
    MyService
  }
})

```

### Context

Service definition can have `expose` object that will expose passed namespace in
service context (`Core.context.services['<identifier>']`) after registering.

This is particuraly useful when creating [templates]().

### Options

Services support options. You can pass an options object to the service definition.
Basically, options are all service needs to be created.
Options object will be passed as first argument to the service `create()`
method at creation. What you do with options depends on your own logic.

For example, a service that just displays a message in the console,
could have **color** or **defaultMessage** option.
So, the `options.js` file should be like:

```js
export default {
  color: '#0000FF',
  defaultMessage: 'Hello world!'
}
```

Then, you can access to options in create method in the `index.js` file:

```js
create: (options, { MyService }, _Core) => {
  // options.color => '#0000FF'
  // options.defaultMessage => 'Hello world!'
  return new MyService(options)
}
```

An options property can be **configurable**, see section below.

### Configuration

As Services have dedicated configuration object, you can make your service configurable
by defining an object under `services['<identifier>']` in the [global configuration](/guide/configuration).

For example, in `config/config.js`:

```js
services: {
  /**
   * Our service configuration object
   */
  'my-service': {
    color: '#0000FF',
    defaultMessage: 'Hello world!'
  }
  /**
   * Other services config
   */
  // ...
}
```

Then, access to service config:

```js
Core.config.services['my-service'].color // => '#0000FF'
Core.config.services['my-service'].defaultMessage // => 'Hello world!'
```

But, what are the differences between service **options** and **configuration**?

Previously, we saw that services **options** are everything the service needs to be **created**, while the **configuration**
is a more user-friendly approach to **configure** the service.
Yes, it does not seem, but it is subtly different.

Let's explain it with an example. Imagine our service uses an external library that
takes its own options. The library have also **color** option with a different
property name (`ex: messageColor`).

In our `options.js` file, let's define the `libOptions` property:

```js
export default {
  color: '#0000FF',
  defaultMessage: 'Hello world!',
  libOptions: {
    messageColor: '#0000FF'
  }
}
```

Next, we have to correctly call our external library in the `create()` method in
`index.js` file:

```js
create: (options, serviceContext, _Core) => {
  const { libOptions, ...myServiceOptions } = options
  return externalLib(libOptions)
}
```

Okay, but why not just get `Core.config.services['my-service'].color`
value in our `create()` method? You can, but you should not because
it is not designed to! Configuration is in a higher level as service options.

:::tip
Work in progress... 🔨
:::

#### Configurable options

You an define a service option as **configurable**. It means that the option
will be a getter for a configuration reference. Use the built-in `makeOptionConfigurable`
tool.

For example, our `color` service option can be mapped to `Core.config.services['my-service'].color`.
Let's define the option as configurable in our `options.js` file:

```js
import { toolbox } from '@hperchec/scorpion-ui'

const { makeConfigurableOption } = toolbox.services

/**
 * Our service option object
 */
const options = {
  // ❌ color: '#0000FF'
  defaultMessage: 'Hello world!'
}

makeConfigurableOption(options, {
  // Options property name. Here, define as `options.color`
  propertyName: 'color',
  // Service identifier
  serviceIdentifier: 'my-service',
  // Path to target from `Core.config.services['<identifier>']`,
  // here: `Core.config.services['<identifier>'].color`
  configPath: 'color',
  // (Optional) Default value
  defaultValue: '#0000FF',
  // (Optional) A formatter function that takes value from config as first parameter.
  // Default return config value as is.
  formatter: (configValue) => configValue,
})

export default options
```

Now, our `color` service option is depending on the service config `color` property.
If you try to assign the option, it will throw an error:

```js
// ❌ Will throw an Error
Core.context.services['my-service'].options.color = '#FFAABB'
```
