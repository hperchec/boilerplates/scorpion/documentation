# Routing

Under the hood, we use the [vue-router](https://v3.router.vuejs.org/) plugin.

Route layout

Override PublicLayout:

```vue
<!--
  Example: src/components/public/PublicLayout.vue
-->
<template>

</template>

<script>
import { Core } from '@hperchec/scorpion-ui'

export default {
  extends: Core.context.vue.components.public.layouts
}
</script>
```