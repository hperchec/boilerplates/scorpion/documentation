# Internationalization

Under the hood, we use the [vue-i18n](https://kazupon.github.io/vue-i18n/introduction.html) plugin.

## Add a language

You can add language translations as you want.