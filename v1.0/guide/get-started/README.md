---
sidebarDepth: 1
---

# Get started

::: tip IMPORTANT
Before doing anything else, please read [documentation](/guide/get-started) about root repository.
:::

## Use boilerplate

First, clone the project:

```sh
git clone https://gitlab.com/hperchec/boilerplates/scorpion/ui
```

Install dependencies:

```sh
npm install
```

Let's run!

```sh
npm run dev
```

## From scratch with Vue CLI

Install `@vue/cli` as global dependency:

```sh
npm install -g @vue/cli
```

Create project based on preset. Replace `<app_name>` by yours:

```sh
vue create -b --preset direct:https://gitlab.com/hperchec/boilerplates/scorpion/lib/vue-cli-preset-scorpion-ui#main -c <app_name>
```