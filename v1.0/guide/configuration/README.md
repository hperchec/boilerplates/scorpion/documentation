# Configuration

ScorpionUI can be configured via the configuration file `config/config.js`.
See also the [default configuration](/guide/api/config/).

## Globals

**Type**: `Object`

The configuration property `globals` contains your app [GLOBALS](/guide/globals):

```js
import GLOBALS from '@/globals.config.json'

export default {
  globals: GLOBALS
}
```

## Vue

**Type**: `Object`

The configuration property `vue` contains Vue specific configuration options.

### rootInstance

**Type**: `Object`

Options for Vue root instance:

```js
export default {
  vue: {
    rootInstance: {
      /* ... */
    }
  }
}
```

#### name

**Type**: `String`

**Default**: `'__ROOT_VUE_INSTANCE__'`

The name of the component and the window property that will be assigned (`window.<name>`).
The value will be assigned to Vue root instance `name` option in `beforeCreate` hook.

```js
export default {
  vue: {
    rootInstance: {
      name: '__ROOT_VUE_INSTANCE__'
    }
  }
}
```

#### targetElement

**Type**: `String`

**Default**: `'#app'`

The element (selector) on which to mount Vue root instance.
The value will be assigned to Vue root instance `el` option in `beforeCreate` hook.

```js
export default {
  vue: {
    rootInstance: {
      targetElement: '#app'
    }
  }
}
```

#### template

**Type**: `String`

**Default**: `'<App/>'`

The Vue root instance template. the value will be assigned to
Vue root instance `template` option in `beforeCreate` hook.

```js
export default {
  vue: {
    rootInstance: {
      template: '<App/>'
    }
  }
}
```

## Services

**Type**: `Object`

The configuration property `services` contains services specific configuration options:

```js
export default {
  services: {
    'router': {
      /* options */
    },
    'store': {
      /* options */
    },
    // etc...
  }
}
```

Read documentation about [services configuration](/guide/services#configuration)
to learn more.

## Developmment

**Type**: `Object`

You can set all root level properties under the `development` key.
It will override the targets in **development** environment:

```js
export default {
  services: {
    'router': {
      /* production options */
    }
  },
  development: {
    services: {
      'router': {
        /* dev specific options */
      }
    }
  }
}
```

