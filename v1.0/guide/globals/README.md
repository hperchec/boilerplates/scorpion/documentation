# Globals

Globals are your app global variables that can be used everywhere, including
by **development** tools.

## Defining globals

These globals are defined in a `globals.config.json`
file in the root folder.

The required globals are the following:

```json
{
  "APP_NAME": "App",
  "VERSION": {
    "CURRENT": "1.0.0"
  }
}
```

Globals are used by Core configuration file. See the [appropriate section](/guide/configuration/).

## Accessing globals

### JS files

Webpack will automatically inject globals in JavaScript code using
[DefinePlugin](https://webpack.js.org/plugins/define-plugin/) as `__GLOBALS__` constant.

```js
console.log(__GLOBALS__.APP_NAME) // => 'App'
```

::: tip See also
- the [formatGlobals](/api/shared/globals/format-globals) shared util documentation
- and the [createConfig](/api/shared/vue/config/create-config) shared util documentation
:::

### Vue files

Each Vue component use the Core [GlobalMixin](/guide/api/context/vue/mixins/global/GlobalMixin) that defines a `GLOBALS` computed
property that references your globals.

In the `<script>` section, you can access globals by the same way as JS files
or via `this.__GLOBALS__`.

> ⚠ In the `<template>`, `__GLOBALS__` will references to `this.__GLOBALS__`

```vue
<template>
  <!-- ✔ Works => 'App' -->
  <div>{{ __GLOBALS__.APP_NAME }}</div>
</template>

<script>
// ✔ Works => 'App'
console.log(__GLOBALS__.APP_NAME)

export default {
  name: 'MyComponent',
  mounted () {
    // ✔ Works => 'App'
    console.log(__GLOBALS__.APP_NAME)
    // ✔ Works => 'App'
    console.log(this.__GLOBALS__.APP_NAME)
  }
}
</script>
```

### During build cycle

Just import/require the json file:

```js
// ESM
import __GLOBALS__ from 'globals.config.json'
// Common
const __GLOBALS__ = require('globals.config.json')
```
