# Dev tools

## Linting

We use [eslint](https://eslint.org/docs/latest/) to lint the source code.

The configuration file is at the root project: `.eslintrc.js`.
