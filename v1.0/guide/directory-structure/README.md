# Directory structure

The directory structure is the following:

```sh
./
├── bin/
│   └── cli/ # CLI
│       ├── cmds/ # Commands
│       ├── lib/
│       └── index.js
├── docs/
├── public/
├── src/
│   ├── assets/
│   ├── components/
│   │   ├── commons/ # global components
│   │   ├── public/
│   │   ├── private/
│   │   └── App.vue # <App> component
│   ├── config/
│   ├── init.js
│   └── main.js
├── tests/
├── www/ # The dist folder
├── config.xml # Cordova configuration
├── globals.config.json # App globals
├── package.json
└── vue.config.js
```

- `bin`: for CLI and build tools
  - `bin/cli`: the CLI folder (see [CLI](/guide/cli/) documentation)
    - `bin/cli/cmds`: the commands folder (see [yargs](https://github.com/yargs/yargs/blob/main/docs/advanced.md#commanddirdirectory-opts)
      documentation)
    - `bin/cli/lib`: libraries and modules used by CLI commands
- `docs`: the documentation folder (see [how to generate documentation](/guide/build/#generate-documentation)
  section)
- `public`: same as Vue project public folder
- `src`: the source code folder
  - `src/assets`: same as Vue project assets folder
  - `src/components`: all app components
    - `src/components/commons`: all components in this directory (and subdirectories)
      will be defined as global component
    - `src/components/public`: the public routes specific components
    - `src/components/private`: the private routes specific components
  - `src/config`: the configuration folder (see [configuration](/guide/configuration)
  - `src/i18n`: the translations folder (see [i18n](/guide/services/i18n)
    service documentation)
  - `src/mixins`: the app mixins folder
  - `src/models`: the app models folder
  - `src/store`: the store folder (see also [store](/guide/services/store)
    service documentation)
  - `src/styles`: the styles folder (see [styles](/guide/internal-flow/#context)
    documentation)
  - `src/context.js`: the context file (see [context](/guide/internal-flow/#context)
    documentation)
  - `src/init.js`: the init file (see [init file](/guide/internal-flow/#init-file)
    documentation)
  - `src/main.js`: the app entry file (see [entry point](/guide/internal-flow/#entrypoint)
    documentation)

::: tip Note
The naming convention is the following:

- directory name: **kebab-case** (ex: `my-folder`)
- `.js` file name: **kebab-case** (ex: `my-file.js`)
- SFC name: **PascalCase** (ex: `MyComponent.vue`)
:::
