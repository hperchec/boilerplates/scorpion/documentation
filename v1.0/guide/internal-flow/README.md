---
sidebarDepth: 1
---

# Internal flow

See also the [API](/guide/api) documentation.

## How it works

Because a picture is worth a thousand words, please refer to the schema below to understand better:

<Mermaid filePath="/guide/internal-flow/_mermaid/how-scorpion-ui-works-with-vue.mmd"/>

Core provides Vue plugin that bring together vue plugins like vue-router etc
Core creates an App that call new Vue

### Init file

The init file is the first module imported by the app entry point.
In this file, the `init()` static method of the `Core` class must be called
by passing Vue as first parameter.

The init file looks like:

```js
import Vue from 'vue'
import { Core } from '@hperchec/scorpion-ui'

/**
 * Initialize Core by passing Vue
 */
Core.init(Vue)
```

### Entrypoint

Here the schema representing the entry point

There are 7 steps that make the entrypoint.

When the app entrypoint (src/main.js by default) is called, the flow is the following:

<Mermaid filePath="/guide/internal-flow/_mermaid/scorpion-ui-entrypoint.mmd"/>

Here's what the entrypoint looks like:

```js
import './init' // First, initialize
import './context' // Then, contextualize
import { Core } from '@hperchec/scorpion-ui'
import config from '#config/config.js' // App config

/**
 * Configure Core
 */
Core.configure(config)

/**
 * main function
 */
async function main () {
  // First, setup
  await Core.setup()

  // Once app is created, services are available
  Core.service('logger').consoleLog('Development mode enabled', { type: 'info', prod: false })

  // Don't forget to call runApp() method
  Core.runApp()
}

/**
 * Run! Once the root Vue instance is created, it will be assigned to
 * window.__ROOT_VUE_INSTANCE__
 */
main()
```

### Context

Now let's explain the _context_ concept.

The `Core.context` object contains everything your app needs
like services (router, store, internationalization...), Vue plugins, components,
mixins, etc...

For example, all necessary data for Vue root instance is under `Core.context.vue.root`.
See also [API documentation](/api/context/vue/).

So, to set what you need and/or to override default context,
do it in the `context.js` file:

```js
import { Core } from '@hperchec/scorpion-ui'
import App from '@/components/App'

// Overwrite App component
Core.context.vue.components.App = App
```

TO DOOOOOOOO:

See also Cookbook for more advanced examples.

### Styles

Default used CSS preprocessor is [Sass](https://sass-lang.com/)

_variables.scss
index.scss
