---
home: true
heroImage: https://v1.vuepress.vuejs.org/hero.png
tagline: Scorpion is one of the fatest ways to create a beautiful cross-platforms SPA
actionText: Quick Start →
actionLink: /guide/
features:
- title: Feature 1 Title
  details: Feature 1 Description
- title: Feature 2 Title
  details: Feature 2 Description
- title: Feature 3 Title
  details: Feature 3 Description
footer: Made by Hervé Perchec with ❤️
---
