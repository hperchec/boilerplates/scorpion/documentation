module.exports = {
  // Sidebar
  '/fr/': [
    {
      title: 'Guide',
      path: '/fr/guide/',
      collapsable: false,
      children: [
        // Get started
        {
          title: 'Get started',
          path: '/fr/guide/get-started/'
        }
      ]
    }
  ]
}