// Node v18 compat
const crypto = require("crypto")
const crypto_orig_createHash = crypto.createHash
crypto.createHash = algorithm => crypto_orig_createHash(algorithm == "md4" ? "sha256" : algorithm)

const path = require('path')

const sidebar_en = require('../sidebar')
const sidebar_fr = require('../fr/sidebar')

module.exports = {
  /**
   * Ref: https://vuepress.vuejs.org/config/#basic-config
   */
  base: '/v1.0/',
  /**
   * Ref: https://vuepress.vuejs.org/config/#dest
   */
  // dest: path.resolve(__dirname, '../../dist/v1.0/'),
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Scorpion UI',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: 'One of the fatest ways to create a beautiful cross-platforms SPA',

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Internationalization
   * 
   * ref: https://vuepress.vuejs.org/guide/i18n.html#site-level-i18n-config
   */
  locales: {
    // The key is the path for the locale to be nested under.
    // As a special case, the default locale can use '/' as its path.
    '/': {
      lang: 'en-US', // this will be set as the lang attribute on <html>
      title: 'Scorpion UI',
      description: 'One of the fatest ways to create a beautiful cross-platforms SPA'
    },
    // 'fr' locale
    '/fr/': {
      lang: 'fr-FR',
      title: 'Scorpion UI',
      description: 'Une des façons les plus simples de créer une SPA multi-plateformes'
    }
  },

  /**
   * Theme: "vt"
   */
  theme: 'vt',

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    // Enable dark mode
    enableDarkMode: true,
    // Repository url
    repo: 'https://gitlab.com/hperchec/boilerplates/scorpion/documentation.git',
    // Disable edit links
    editLinks: false,
    editLinkText: 'Edit this page on GitHub',
    // Display last update
    lastUpdated: true,
    // Internationalization
    locales: {
      // 'en' locale
      '/': {
        // Navbar button label for translations
        selectText: 'Language',
        label: 'English',
        // Aria Label for locale in the dropdown
        ariaLabel: 'Languages',
        // Navigation
        nav: [
          { text: 'Guide', link: '/guide/' },
          { text: 'Cookbook', link: '/cookbook/' },
          { text: 'API (en)', link: '/api/' }
        ],
        sidebar: sidebar_en
      },
      // 'fr' locale
      '/fr/': {
        // Navbar button label for translations
        selectText: 'Langue',
        label: 'Français',
        // Aria Label for locale in the dropdown
        ariaLabel: 'Langues',
        // Navigation
        nav: [
          { text: 'Guide', link: '/fr/guide/' },
          { text: 'Guide', link: '/fr/guide/' },
          { text: 'Cookbook', link: '/fr/cookbook/' },
          { text: 'API (en)', link: '/api/' }
        ],
        sidebar: sidebar_fr
      }
    }
  },

  /**
   * Markdown options
   * 
   * ref: https://vuepress.vuejs.org/guide/markdown.html#line-numbers
   */
  markdown: {
    anchor: {
      permalinkSymbol: '<i class="fa-solid fa-hashtag"></i>' // Replace the hashtag '#' before each header
    },
    lineNumbers: true
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom'
  ],

  // Overwrite webpack config
  chainWebpack: config => {
    config.module
      .rule('mmd')
      .test(/\.mmd$/)
      .exclude
        .add(/node_modules/)
        .end()
      .use('raw-loader')
        .loader('raw-loader')
  }
}
