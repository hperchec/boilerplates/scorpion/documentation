/**
 * See full list of options here: https://mermaid-js.github.io/mermaid/#/Setup?id=mermaidapi-configuration-defaults
 */
const config = {
  theme: 'base',
  startOnLoad: true,
  flowchart: {
    htmlLabels: true
  },
  themeVariables: {
    fontFamily: 'inherit',
    primaryTextColor: '#191919',
    secondaryTextColor: '#191919',
    tertiaryTextColor: '#191919',
    lineColor: '#F65D6C',
    nodeBorder: '#F65D6C',
    primaryBorderColor: '#F65D6C',
    secondaryBorderColor: '#F65D6C',
    tertiaryBorderColor: '#F65D6C',
    edgeLabelBackground: 'inherit'
  }
}

export default config