import mermaid from 'mermaid'
// Mermaid config
import mermaidConfig from './config'

// Require context: find all .mmd files
export const mermaidFiles = {}

function importAll (r) {
  r.keys().forEach(key => {
    // Remove the first dot ('.') at the start of the string
    const fileName = key.slice(1)
    // Add to mermaidFiles
    mermaidFiles[fileName] = r(key).default
  })
}
const ctx = require.context('../../', true, /.*\.mmd$/)
importAll(ctx)

mermaid.initialize(mermaidConfig)

export default mermaidFiles
