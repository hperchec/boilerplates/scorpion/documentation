module.exports = [
    {
        "title": "Default configuration",
        "children": [],
        "path": "/api/config/",
        "initialOpenGroupIndex": 0
    },
    {
        "title": "Context",
        "children": [
            {
                "title": "Support",
                "children": [
                    {
                        "title": "collection",
                        "children": [
                            {
                                "title": "Collection class",
                                "initialOpenGroupIndex": 0,
                                "path": "/api/context/support/collection/Collection"
                            }
                        ],
                        "path": "/api/context/support/collection/",
                        "initialOpenGroupIndex": 0
                    },
                    {
                        "title": "model",
                        "children": [
                            {
                                "title": "Model class",
                                "initialOpenGroupIndex": 0,
                                "path": "/api/context/support/model/Model"
                            }
                        ],
                        "path": "/api/context/support/model/",
                        "initialOpenGroupIndex": 0
                    }
                ],
                "path": "/api/context/support/",
                "initialOpenGroupIndex": -1
            },
            {
                "title": "Vue",
                "children": [
                    {
                        "title": "Components",
                        "children": [
                            {
                                "title": "App",
                                "initialOpenGroupIndex": 0,
                                "path": "/api/context/vue/components/App"
                            },
                            {
                                "title": "Commons",
                                "children": [
                                    {
                                        "title": "Page",
                                        "initialOpenGroupIndex": 0,
                                        "path": "/api/context/vue/components/commons/Page"
                                    },
                                    {
                                        "title": "ScorpionLogo",
                                        "initialOpenGroupIndex": 0,
                                        "path": "/api/context/vue/components/commons/ScorpionLogo"
                                    }
                                ],
                                "path": "/api/context/vue/components/commons/",
                                "initialOpenGroupIndex": 0
                            }
                        ],
                        "path": "/api/context/vue/components/",
                        "initialOpenGroupIndex": -1
                    },
                    {
                        "title": "directives",
                        "children": [],
                        "path": "/api/context/vue/directives/",
                        "initialOpenGroupIndex": 0
                    },
                    {
                        "title": "Filters",
                        "children": [],
                        "path": "/api/context/vue/filters/",
                        "initialOpenGroupIndex": 0
                    },
                    {
                        "title": "Mixins",
                        "children": [
                            {
                                "title": "global",
                                "children": [
                                    {
                                        "title": "GlobalMixin",
                                        "initialOpenGroupIndex": 0,
                                        "path": "/api/context/vue/mixins/global/GlobalMixin"
                                    }
                                ],
                                "path": "/api/context/vue/mixins/global/",
                                "initialOpenGroupIndex": 0
                            },
                            {
                                "title": "VariantableMixin",
                                "initialOpenGroupIndex": 0,
                                "path": "/api/context/vue/mixins/VariantableMixin"
                            },
                            {
                                "title": "VModelableMixin",
                                "initialOpenGroupIndex": 0,
                                "path": "/api/context/vue/mixins/VModelableMixin"
                            }
                        ],
                        "path": "/api/context/vue/mixins/",
                        "initialOpenGroupIndex": -1
                    },
                    {
                        "title": "Plugins",
                        "children": [
                            {
                                "title": "core-plugin",
                                "children": [],
                                "path": "/api/context/vue/plugins/core-plugin/",
                                "initialOpenGroupIndex": 0
                            }
                        ],
                        "path": "/api/context/vue/plugins/",
                        "initialOpenGroupIndex": -1
                    },
                    {
                        "title": "Root instance",
                        "children": [
                            {
                                "title": "Options",
                                "initialOpenGroupIndex": 0,
                                "path": "/api/context/vue/root/options"
                            },
                            {
                                "title": "prototype",
                                "children": [
                                    {
                                        "title": "app",
                                        "children": [],
                                        "path": "/api/context/vue/root/prototype/app/",
                                        "initialOpenGroupIndex": 0
                                    }
                                ],
                                "path": "/api/context/vue/root/prototype/",
                                "initialOpenGroupIndex": 0
                            }
                        ],
                        "path": "/api/context/vue/root/",
                        "initialOpenGroupIndex": -1
                    }
                ],
                "path": "/api/context/vue/",
                "initialOpenGroupIndex": -1
            }
        ],
        "path": "/api/context/",
        "initialOpenGroupIndex": -1
    },
    {
        "title": "Built-in services",
        "children": [
            {
                "title": "api-manager",
                "children": [
                    {
                        "title": "API",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/api-manager/API"
                    },
                    {
                        "title": "APIManager",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/api-manager/APIManager"
                    },
                    {
                        "title": "APIOptions",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/api-manager/APIOptions"
                    },
                    {
                        "title": "Configuration",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/api-manager/config"
                    },
                    {
                        "title": "Options",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/api-manager/options"
                    }
                ],
                "path": "/api/services/api-manager/",
                "initialOpenGroupIndex": -1
            },
            {
                "title": "event-bus",
                "children": [
                    {
                        "title": "EventBus",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/event-bus/EventBus"
                    },
                    {
                        "title": ".options",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/event-bus/options"
                    }
                ],
                "path": "/api/services/event-bus/",
                "initialOpenGroupIndex": -1
            },
            {
                "title": "i18n",
                "children": [
                    {
                        "title": "Configuration",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/i18n/config"
                    },
                    {
                        "title": ".messages",
                        "children": [
                            {
                                "title": "en",
                                "children": [],
                                "path": "/api/services/i18n/messages/en/",
                                "initialOpenGroupIndex": 0
                            },
                            {
                                "title": "fr",
                                "children": [],
                                "path": "/api/services/i18n/messages/fr/",
                                "initialOpenGroupIndex": 0
                            }
                        ],
                        "path": "/api/services/i18n/messages/",
                        "initialOpenGroupIndex": 0
                    },
                    {
                        "title": ".options",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/i18n/options"
                    }
                ],
                "path": "/api/services/i18n/",
                "initialOpenGroupIndex": -1
            },
            {
                "title": "local-storage-manager",
                "children": [
                    {
                        "title": "Configuration",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/local-storage-manager/config"
                    },
                    {
                        "title": "LocalStorageManager",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/local-storage-manager/LocalStorageManager"
                    },
                    {
                        "title": ".options",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/local-storage-manager/options"
                    }
                ],
                "path": "/api/services/local-storage-manager/",
                "initialOpenGroupIndex": -1
            },
            {
                "title": "logger",
                "children": [
                    {
                        "title": "Configuration",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/logger/config"
                    },
                    {
                        "title": "Logger",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/logger/Logger"
                    },
                    {
                        "title": ".options",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/logger/options"
                    }
                ],
                "path": "/api/services/logger/",
                "initialOpenGroupIndex": -1
            },
            {
                "title": "router",
                "children": [
                    {
                        "title": "components",
                        "children": [
                            {
                                "title": "private",
                                "children": [
                                    {
                                        "title": "views",
                                        "children": [],
                                        "path": "/api/services/router/components/private/views/",
                                        "initialOpenGroupIndex": 0
                                    }
                                ],
                                "path": "/api/services/router/components/private/",
                                "initialOpenGroupIndex": 0
                            },
                            {
                                "title": "public",
                                "children": [
                                    {
                                        "title": "views",
                                        "children": [
                                            {
                                                "title": "home",
                                                "children": [
                                                    {
                                                        "title": "Index",
                                                        "initialOpenGroupIndex": 0,
                                                        "path": "/api/services/router/components/public/views/home/_Index"
                                                    }
                                                ],
                                                "path": "/api/services/router/components/public/views/home/",
                                                "initialOpenGroupIndex": 0
                                            },
                                            {
                                                "title": "not-found",
                                                "children": [
                                                    {
                                                        "title": "Index",
                                                        "initialOpenGroupIndex": 0,
                                                        "path": "/api/services/router/components/public/views/not-found/_Index"
                                                    }
                                                ],
                                                "path": "/api/services/router/components/public/views/not-found/",
                                                "initialOpenGroupIndex": 0
                                            }
                                        ],
                                        "path": "/api/services/router/components/public/views/",
                                        "initialOpenGroupIndex": 0
                                    }
                                ],
                                "path": "/api/services/router/components/public/",
                                "initialOpenGroupIndex": 0
                            }
                        ],
                        "path": "/api/services/router/components/",
                        "initialOpenGroupIndex": 0
                    },
                    {
                        "title": "Configuration",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/router/config"
                    },
                    {
                        "title": ".middlewares",
                        "children": [
                            {
                                "title": ".AppMiddleware",
                                "initialOpenGroupIndex": 0,
                                "path": "/api/services/router/middlewares/AppMiddleware"
                            }
                        ],
                        "path": "/api/services/router/middlewares/",
                        "initialOpenGroupIndex": 0
                    },
                    {
                        "title": "mixins",
                        "children": [
                            {
                                "title": "RouteComponentMixin",
                                "initialOpenGroupIndex": 0,
                                "path": "/api/services/router/mixins/RouteComponentMixin"
                            }
                        ],
                        "path": "/api/services/router/mixins/",
                        "initialOpenGroupIndex": 0
                    },
                    {
                        "title": "Options",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/router/options"
                    },
                    {
                        "title": ".PrivateRoute",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/router/PrivateRoute"
                    },
                    {
                        "title": ".PublicRoute",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/router/PublicRoute"
                    },
                    {
                        "title": ".Route",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/router/Route"
                    },
                    {
                        "title": ".routes",
                        "children": [
                            {
                                "title": ".private",
                                "children": [],
                                "path": "/api/services/router/routes/private/",
                                "initialOpenGroupIndex": 0
                            },
                            {
                                "title": ".public",
                                "children": [
                                    {
                                        "title": ".Home",
                                        "initialOpenGroupIndex": 0,
                                        "path": "/api/services/router/routes/public/Home"
                                    },
                                    {
                                        "title": ".NotFound",
                                        "initialOpenGroupIndex": 0,
                                        "path": "/api/services/router/routes/public/NotFound"
                                    },
                                    {
                                        "title": "._CatchAll",
                                        "initialOpenGroupIndex": 0,
                                        "path": "/api/services/router/routes/public/_CatchAll"
                                    }
                                ],
                                "path": "/api/services/router/routes/public/",
                                "initialOpenGroupIndex": 0
                            }
                        ],
                        "path": "/api/services/router/routes/",
                        "initialOpenGroupIndex": 0
                    }
                ],
                "path": "/api/services/router/",
                "initialOpenGroupIndex": -1
            },
            {
                "title": "store",
                "children": [
                    {
                        "title": "modules",
                        "children": [
                            {
                                "title": "resources",
                                "children": [
                                    {
                                        "title": "options",
                                        "initialOpenGroupIndex": 0,
                                        "path": "/api/services/store/modules/resources/options"
                                    },
                                    {
                                        "title": "tools",
                                        "children": [
                                            {
                                                "title": "create-module",
                                                "initialOpenGroupIndex": 0,
                                                "path": "/api/services/store/modules/resources/tools/create-module"
                                            },
                                            {
                                                "title": "generate-actions",
                                                "initialOpenGroupIndex": 0,
                                                "path": "/api/services/store/modules/resources/tools/generate-actions"
                                            },
                                            {
                                                "title": "generate-getters",
                                                "initialOpenGroupIndex": 0,
                                                "path": "/api/services/store/modules/resources/tools/generate-getters"
                                            },
                                            {
                                                "title": "generate-mutations",
                                                "initialOpenGroupIndex": 0,
                                                "path": "/api/services/store/modules/resources/tools/generate-mutations"
                                            },
                                            {
                                                "title": "generate-state",
                                                "initialOpenGroupIndex": 0,
                                                "path": "/api/services/store/modules/resources/tools/generate-state"
                                            }
                                        ],
                                        "path": "/api/services/store/modules/resources/tools/",
                                        "initialOpenGroupIndex": 0
                                    }
                                ],
                                "path": "/api/services/store/modules/resources/",
                                "initialOpenGroupIndex": 0
                            },
                            {
                                "title": "system",
                                "children": [
                                    {
                                        "title": "options",
                                        "initialOpenGroupIndex": 0,
                                        "path": "/api/services/store/modules/system/options"
                                    }
                                ],
                                "path": "/api/services/store/modules/system/",
                                "initialOpenGroupIndex": 0
                            }
                        ],
                        "path": "/api/services/store/modules/",
                        "initialOpenGroupIndex": 0
                    },
                    {
                        "title": ".options",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/store/options"
                    },
                    {
                        "title": "plugins",
                        "children": [
                            {
                                "title": "system-plugin",
                                "children": [
                                    {
                                        "title": "options",
                                        "initialOpenGroupIndex": 0,
                                        "path": "/api/services/store/plugins/system-plugin/options"
                                    }
                                ],
                                "path": "/api/services/store/plugins/system-plugin/",
                                "initialOpenGroupIndex": 0
                            }
                        ],
                        "path": "/api/services/store/plugins/",
                        "initialOpenGroupIndex": 0
                    }
                ],
                "path": "/api/services/store/",
                "initialOpenGroupIndex": -1
            },
            {
                "title": "toast-manager",
                "children": [
                    {
                        "title": "Configuration",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/toast-manager/config"
                    },
                    {
                        "title": "Options",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/toast-manager/options"
                    },
                    {
                        "title": "Toast",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/toast-manager/Toast"
                    },
                    {
                        "title": "Toaster",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/toast-manager/Toaster"
                    },
                    {
                        "title": "ToastManager",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/services/toast-manager/ToastManager"
                    }
                ],
                "path": "/api/services/toast-manager/",
                "initialOpenGroupIndex": -1
            }
        ],
        "path": "/api/services/",
        "initialOpenGroupIndex": -1
    },
    {
        "title": "Toolbox",
        "children": [
            {
                "title": "Hook",
                "initialOpenGroupIndex": 0,
                "path": "/api/toolbox/Hook"
            },
            {
                "title": "services",
                "children": [
                    {
                        "title": "make-configurable-option",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/toolbox/services/make-configurable-option"
                    }
                ],
                "path": "/api/toolbox/services/",
                "initialOpenGroupIndex": 0
            },
            {
                "title": "template",
                "children": [
                    {
                        "title": "utils",
                        "children": [],
                        "path": "/api/toolbox/template/utils/",
                        "initialOpenGroupIndex": 0
                    }
                ],
                "path": "/api/toolbox/template/",
                "initialOpenGroupIndex": 0
            },
            {
                "title": "vue",
                "children": [
                    {
                        "title": ".VuePluginable",
                        "initialOpenGroupIndex": 0,
                        "path": "/api/toolbox/vue/VuePluginable"
                    }
                ],
                "path": "/api/toolbox/vue/",
                "initialOpenGroupIndex": 0
            }
        ],
        "path": "/api/toolbox/",
        "initialOpenGroupIndex": -1
    },
    {
        "title": "Utils",
        "children": [
            {
                "title": "camelcase",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/camelcase"
            },
            {
                "title": "capitalize",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/capitalize"
            },
            {
                "title": "cloneDeep",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/clone-deep"
            },
            {
                "title": "clone",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/clone"
            },
            {
                "title": "completeAssign",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/complete-assign"
            },
            {
                "title": "constantcase",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/constantcase"
            },
            {
                "title": "createColor",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/create-color"
            },
            {
                "title": "dateFormat",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/date-format"
            },
            {
                "title": "debounce",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/debounce"
            },
            {
                "title": "deepMerge",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/deep-merge"
            },
            {
                "title": "getConstructorName",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/get-constructor-name"
            },
            {
                "title": "getHostHttpUrl",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/get-host-http-url"
            },
            {
                "title": "getPrototypeChain",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/get-prototype-chain"
            },
            {
                "title": "getPrototypeNames",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/get-prototype-names"
            },
            {
                "title": "get",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/get"
            },
            {
                "title": "has",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/has"
            },
            {
                "title": "isArray",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-array"
            },
            {
                "title": "isBlankString",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-blank-string"
            },
            {
                "title": "isDef",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-def"
            },
            {
                "title": "isEmptyString",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-empty-string"
            },
            {
                "title": "isEqual",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-equal"
            },
            {
                "title": "isFalse",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-false"
            },
            {
                "title": "isNull",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-null"
            },
            {
                "title": "isObject",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-object"
            },
            {
                "title": "isPlainObject",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-plain-object"
            },
            {
                "title": "isPrimitive",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-primitive"
            },
            {
                "title": "isPromise",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-promise"
            },
            {
                "title": "isRegExp",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-regexp"
            },
            {
                "title": "isTrue",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-true"
            },
            {
                "title": "isUndef",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-undef"
            },
            {
                "title": "isValidArrayIndex",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-valid-array-index"
            },
            {
                "title": "isValidHttpUrl",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/is-valid-http-url"
            },
            {
                "title": "kebabcase",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/kebabcase"
            },
            {
                "title": "lowercase",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/lowercase"
            },
            {
                "title": "maxBy",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/max-by"
            },
            {
                "title": "minBy",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/min-by"
            },
            {
                "title": "random",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/random"
            },
            {
                "title": "safeAsync",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/safe-async"
            },
            {
                "title": "safe",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/safe"
            },
            {
                "title": "set",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/set"
            },
            {
                "title": "snakecase",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/snakecase"
            },
            {
                "title": "sortBy",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/sort-by"
            },
            {
                "title": "toNumber",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/to-number"
            },
            {
                "title": "toRawType",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/to-raw-type"
            },
            {
                "title": "typeCheck",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/type-check"
            },
            {
                "title": "union",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/union"
            },
            {
                "title": "uppercase",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/uppercase"
            },
            {
                "title": "validateEmail",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/validate-email"
            },
            {
                "title": "XArray",
                "initialOpenGroupIndex": 0,
                "path": "/api/utils/x-array"
            }
        ],
        "path": "/api/utils/",
        "initialOpenGroupIndex": 0
    },
    {
        "title": "Core",
        "initialOpenGroupIndex": 0,
        "path": "/api/Core"
    },
    {
        "title": "Service",
        "initialOpenGroupIndex": 0,
        "path": "/api/Service"
    },
    {
        "title": "ServiceDefinition",
        "initialOpenGroupIndex": 0,
        "path": "/api/ServiceDefinition"
    }
]