---
title: Default configuration
headline: Default configuration
sidebarTitle: Default configuration
sidebarDepth: 0
prev: ../
next: ../context/
---

# Default configuration

<a name="config">
</a>

## config : <code>object</code>

> **Ref.** : [Core](../Core).[config](./)This is the default configuration<<< @/v1.0/api/config/index.source.js#snippet

