/**
 * @vuepress
 * ---
 * title: Default configuration
 * headline: Default configuration
 * sidebarTitle: Default configuration
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: ../
 * next: ../context/
 * ---
 */

/**
 * @name config
 * @type {object}
 * @description
 * > **Ref.** : {@link ../Core Core}.{@link ./ config}
 *
 * This is the default configuration
 *
 * <<< @/{{{{VUEPRESS_BASE_PATH}}}}{{{{VUEPRESS_TARGET_PATH}}}}/config/index.source.js#snippet
 */

export default
// #region snippet
{
  /**
   * Define app globals
   */
  globals: {
    APP_NAME: 'App',
    VERSION: {
      CURRENT: '1.0.0'
    }
  },
  /**
   * Vue specific configuration options
   */
  vue: {
    /**
     * Options for Vue root instance
     */
    root: {
      /**
       * The name of the component and the window property that will be assigned (`window.<name>`)
       */
      name: '__ROOT_VUE_INSTANCE__',
      /**
       * The element (selector) on which to mount Vue root instance
       */
      targetElement: '#app',
      /**
       * The Vue root instance template
       */
      template: '<App/>'
    }
  },
  /**
   * Services configuration
   */
  services: {
    /**
     * Built-in 'api-manager' service default configuration
     */
    'api-manager': {
      apis: {
        // Default server API
        ServerAPI: {
          baseURL: '/api',
          allowedMethods: [ 'GET', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS' ],
          headers: {
            'X-Requested-With': 'XMLHttpRequest'
          }
        }
      }
    },
    /**
     * Built-in 'i18n' service default configuration
     */
    'i18n': {
      availableLocales: [ 'en', 'fr' ],
      defaultLocale: 'en',
      fallbackLocale: 'en'
    },
    /**
     * Built-in 'local-storage-manager' service default configuration
     */
    'local-storage-manager': {
      setItemEventName: 'ls-item-set',
      deleteItemEventName: 'ls-item-removed'
    },
    /**
     * Built-in 'logger' service default configuration
     */
    'logger': {
      log: true,
      types: {
        info: {
          badgeContent: '[INFO]',
          badgeColor: '#FDE5DC',
          badgeBgColor: '#F58E69',
          messageColor: '#A4330B',
          prependMessage: '❔'
        },
        advice: {
          badgeContent: '[ADVICE]',
          badgeColor: '#B0E9DD',
          badgeBgColor: '#2FA88F',
          messageColor: '#185448',
          prependMessage: '💡'
        },
        warning: {
          badgeContent: '[WARNING]',
          badgeColor: '#FFFAEB',
          badgeBgColor: '#F5B800',
          messageColor: '#936E00',
          prependMessage: '⚠'
        },
        error: {
          badgeContent: '[ERROR]',
          badgeColor: '#F87D89',
          badgeBgColor: '#A82F3B',
          messageColor: '#F21D32',
          prependMessage: '❌'
        },
        system: {
          badgeContent: '[SYSTEM]',
          badgeColor: '#FDDDE1',
          badgeBgColor: '#F65D6C',
          messageColor: '#A10918',
          prependMessage: '🚀'
        },
        router: {
          badgeContent: '[ROUTER]',
          badgeColor: '#AFD3E4',
          badgeBgColor: '#3883A8',
          messageColor: '#1C4254',
          prependMessage: '🚦'
        },
        store: {
          badgeContent: '[STORE]',
          badgeColor: '#BDC6FB',
          badgeBgColor: '#5168F5',
          messageColor: '#091D9A',
          prependMessage: '☁'
        }
      }
    },
    /**
     * Built-in 'router' service default configuration
     */
    'router': {
      catchAllRedirect: '/404'
    },
    /**
     * Built-in 'toast-manager' service default configuration
     */
    'toast-manager': {
      toasters: {
        // Default 'Mainframe' toaster
        Mainframe: {}
      }
    }
  },
  /**
   * Development environment specific configuration
   */
  development: process.env.NODE_ENV === 'development' ? {
    services: {
      'api-manager': {
        ServerAPI: {
          URL: 'http://localhost:8000', // Local server url
          baseURL: 'http://localhost:8000/api'
        }
      },
      'logger': {
        log: true
      }
    }
  } : {}
}
// #endregion snippet
