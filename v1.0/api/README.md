---
title: ScorpionUI API
headline: ScorpionUI API
prev: false
next: ./config/
---

# ScorpionUI API

<a name="module-scorpionui">
</a>

## ScorpionUI

Scorpion UI module```javascriptimport ScorpionUI from '@hperchec/scorpion-ui'```

**Author**: Hervé Perchec <contact@herve-perchec.com>  
**Schema**:

- [ScorpionUI](#module-scorpionui)
  - [module.exports](#expmodulescorpionui-module-exports) : <code>object</code> ⏏
    - [.Core](#module-scorpionui-module-exports-core) : <code>function</code>
    - [.builtInServices](#module-scorpionui-module-exports-builtinservices) : <code>object</code>
    - [.Service](#module-scorpionui-module-exports-service) : <code>function</code>
    - [.ServiceDefinition](#module-scorpionui-module-exports-servicedefinition) : <code>function</code>
    - [.utils](#module-scorpionui-module-exports-utils) : <code>object</code>
    - [.toolbox](#module-scorpionui-module-exports-toolbox) : <code>object</code>

<a name="expmodulescorpionui-module-exports" data-sidebar-title=".exports (default)"></a>

### module.exports : <code>object</code> ⏏

scorpion-ui module

<a name="module-scorpionui-module-exports-core">
</a>

#### ScorpionUI.Core : <code>function</code>

The Core class

**See**: [./Core](./Core)

<a name="module-scorpionui-module-exports-builtinservices">
</a>

#### ScorpionUI.builtInServices : <code>object</code>

The built-in services

**See**: [./services](./services)

<a name="module-scorpionui-module-exports-service">
</a>

#### ScorpionUI.Service : <code>function</code>

The Service class

**See**: [./Service](./Service)

<a name="module-scorpionui-module-exports-servicedefinition">
</a>

#### ScorpionUI.ServiceDefinition : <code>function</code>

The ServiceDefinition class

**See**: [./ServiceDefinition](./ServiceDefinition)

<a name="module-scorpionui-module-exports-utils">
</a>

#### ScorpionUI.utils : <code>object</code>

The default Core utils

**See**: [./utils](./utils)

<a name="module-scorpionui-module-exports-toolbox">
</a>

#### ScorpionUI.toolbox : <code>object</code>

The lib toolbox

**See**: [./toolbox](./toolbox)

