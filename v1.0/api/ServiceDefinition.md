---
title: ServiceDefinition class
headline: ServiceDefinition class
sidebarTitle: ServiceDefinition
sidebarDepth: 0
prev: ./Service
next: false
---

# ServiceDefinition class

<a name="servicedefinition">
</a>

## ServiceDefinition

> [context](../).[services](./).[ServiceDefinition](#)ServiceDefinition class

**Schema**:

- [ServiceDefinition](#servicedefinition)
  - [new ServiceDefinition(def)](#newservicedefinitionnew)
  - [.identifier](#servicedefinition-identifier) : <code>string</code>
  - [.create](#servicedefinition-create) : <code>function</code>
  - [.register](#servicedefinition-register) : <code>function</code>
  - [.vuePlugin](#servicedefinition-vueplugin) : <code>function</code>
  - [.pluginOptions](#servicedefinition-pluginoptions) : <code>\*</code>
  - [.rootOption](#servicedefinition-rootoption) : <code>function</code> \| <code>string</code>

<a name="newservicedefinitionnew">
</a>

### new ServiceDefinition(def)

Create a ServiceDefinition instance

| Param | Type | Description |
| --- | --- | --- |
| def | <code>object</code> | The service definition object |
| def.identifier | <code>string</code> | The service identifier |
| def.create | <code>function</code> | The function that returns the service instance / object. |
| [def.register] | <code>function</code> | The function to call when registering the service. |
| [def.vuePlugin] | <code>function</code> | Returns the service Vue plugin object |
| [def.pluginOptions] | <code>\*</code> | If `vuePlugin` is defined, will be passed as Vue plugin options |
| [def.rootOption] | <code>function</code> \| <code>string</code> | Returns rootOption string. If defined, the service will be passed to Vue root instance options |

<a name="servicedefinition-identifier">
</a>

### serviceDefinition.identifier : <code>string</code>

Service identifier

**Category**: properties  
<a name="servicedefinition-create">
</a>

### serviceDefinition.create : <code>function</code>

Function that creates the service

**Category**: properties  
<a name="servicedefinition-register">
</a>

### serviceDefinition.register : <code>function</code>

Function to call when registering the service

**Category**: properties  
<a name="servicedefinition-vueplugin">
</a>

### serviceDefinition.vuePlugin : <code>function</code>

A function that returns vue plugin

**Category**: properties  
<a name="servicedefinition-pluginoptions">
</a>

### serviceDefinition.pluginOptions : <code>\*</code>

The Vue plugin options

**Category**: properties  
<a name="servicedefinition-rootoption">
</a>

### serviceDefinition.rootOption : <code>function</code> \| <code>string</code>

The Vue root instance option property name

**Category**: properties  
