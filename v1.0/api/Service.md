---
title: Service class
headline: Service class
sidebarTitle: Service
sidebarDepth: 0
prev: ./Core
next: ./ServiceDefinition
---

# Service class

<a name="service">
</a>

## Service

Service class```jsimport { Service } from '@hperchec/scorpion-ui'```

**Schema**:

- [Service](#service)
  - [new Service(serviceObject)](#newservicenew)
  - _instance_
    - _methods_
      - [.getDefinition()](#service-getdefinition) ⇒ <code>ServiceDefinition</code>
      - [.getNamespacedProperties()](#service-getnamespacedproperties) ⇒ <code>Array.&lt;string&gt;</code>
      - [.getNamespace()](#service-getnamespace) ⇒ <code>object</code>
    - _properties_
      - [.options](#service-options) : <code>object</code>
  - _static_
    - _static methods_
      - [.resolveServiceDef(def)](#service-resolveservicedef) ⇒ <code>ServiceDefinition</code>

<a name="newservicenew">
</a>

### new Service(serviceObject)

Service class

| Param | Type | Description |
| --- | --- | --- |
| serviceObject | <code>object</code> | The service object |
| serviceObject.service | <code>object</code> | The service definition |
| [serviceObject.options] | <code>object</code> | The service options object |
| [serviceObject.expose] | <code>object</code> | The object to expose in service context |

<a name="service-getdefinition">
</a>

### service.getDefinition() ⇒ <code>ServiceDefinition</code>

Returns the service definition

**Returns**: <code>ServiceDefinition</code> - Returns the service definition

**Category**: methods  
<a name="service-getnamespacedproperties">
</a>

### service.getNamespacedProperties() ⇒ <code>Array.&lt;string&gt;</code>

Returns the namespaced keys (from 'expose')

**Returns**: <code>Array.&lt;string&gt;</code> - Returns the namespaced keys (from 'expose')

**Category**: methods  
<a name="service-getnamespace">
</a>

### service.getNamespace() ⇒ <code>object</code>

Returns the exposed object

**Returns**: <code>object</code> - Returns the exposed object

**Category**: methods  
<a name="service-options">
</a>

### service.options : <code>object</code>

Service options

**Category**: properties  
<a name="service-resolveservicedef">
</a>

### Service.resolveServiceDef(def) ⇒ <code>ServiceDefinition</code>

Resolve a service definition

**Returns**: <code>ServiceDefinition</code> - Returns the service definition

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| def | <code>object</code> | The service definition object to pass to ServiceDefinition constructor |

