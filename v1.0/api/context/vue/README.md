---
title: Vue
headline: Vue
sidebarTitle: Vue
initialOpenGroupIndex: -1
prev: ../
next: ./components/
---

# Vue

<a name="vue">
</a>

## vue : <code>object</code>

Contains all relative Vue data (components, mixins, etc...)

**Schema**:

- [vue](#vue) : <code>object</code>
  - [.components](#vue-components) : <code>object</code>
  - [.directives](#vue-directives) : <code>object</code>
  - [.filters](#vue-filters) : <code>object</code>
  - [.mixins](#vue-mixins) : <code>object</code>
  - [.plugins](#vue-plugins) : <code>object</code>
  - [.root](#vue-root) : <code>object</code>

<a name="vue-components">
</a>

### Core.context.vue.components : <code>object</code>

**See**: [components](./components)

<a name="vue-directives">
</a>

### Core.context.vue.directives : <code>object</code>

**See**: [directives](./directives)

<a name="vue-filters">
</a>

### Core.context.vue.filters : <code>object</code>

**See**: [filters](./filters)

<a name="vue-mixins">
</a>

### Core.context.vue.mixins : <code>object</code>

**See**: [mixins](./mixins)

<a name="vue-plugins">
</a>

### Core.context.vue.plugins : <code>object</code>

**See**: [plugins](./plugins)

<a name="vue-root">
</a>

### Core.context.vue.root : <code>object</code>

**See**: [root](./root)

