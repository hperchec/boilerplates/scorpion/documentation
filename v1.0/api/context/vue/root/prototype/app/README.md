---
title: __index__
headline: __index__
---

# __index__

## Constants

<dl>
<dt><a href="#device">device</a></dt>
<dd><p>Detect device
<code>this.$app.device</code></p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#userSettings">userSettings()</a> ⇒ <code>object</code></dt>
<dd><p>Get user settings (<code>this.$app.userSettings</code>)</p>
</dd>
</dl>

<a name="device">
</a>

## device

Detect device`this.$app.device`

<a name="usersettings">
</a>

## userSettings() ⇒ <code>object</code>

Get user settings (`this.$app.userSettings`)

**Returns**: <code>object</code> - Returns root instance data 'userSettings' value

