---
title: Options
headline: Options
sidebarTitle: Options
sidebarDepth: 0
prev: ./prototype/
next: false
---

# Options

<a name="options">
</a>

## options

Vue root instance options

**Schema**:

- [options](#options)
  - [.mixins](#options-mixins) : <code>Array.&lt;object&gt;</code>
  - [.components](#options-components) : <code>object</code>
  - [.methods](#options-methods) : <code>object</code>
    - [.initUserSettings()](#options-methods-initusersettings) ⇒ <code>void</code>
  - [.created](#options-created) : <code>function</code>
  - [.data()](#options-data) : <code>function</code>
    - [.shared](#options-data-shared) : <code>object</code>
      - [.GLOBALS](#options-data-shared-globals) : <code>object</code>
    - [.userSettings](#options-data-usersettings) : <code>object</code>
  - [.beforeCreate()](#options-beforecreate) : <code>function</code>
  - [.mounted()](#options-mounted) : <code>function</code>

<a name="options-mixins">
</a>

### Core.context.vue.root.options.mixins : <code>Array.&lt;object&gt;</code>

Vue root instance mixins options

**Default**: <code>[]</code>  
<a name="options-components">
</a>

### Core.context.vue.root.options.components : <code>object</code>

Vue root instance components options

<a name="options-methods">
</a>

### Core.context.vue.root.options.methods : <code>object</code>

Vue root instance methods

<a name="options-methods-initusersettings">
</a>

#### methods.initUserSettings() ⇒ <code>void</code>

Initialize user settings

<a name="options-created">
</a>

### Core.context.vue.root.options.created : <code>function</code>

Vue root instance created hookSee also: how to customize root instance hooks documentation.Custom merge strategy is applied for root instance created hook: other created hooksfrom mixins will be ADDED AFTER this function

<a name="options-data">
</a>

### Core.context.vue.root.options.data() : <code>function</code>

Vue root instance data options

**Schema**:

- [.data()](#options-data) : <code>function</code>
  - [.shared](#options-data-shared) : <code>object</code>
    - [.GLOBALS](#options-data-shared-globals) : <code>object</code>
  - [.userSettings](#options-data-usersettings) : <code>object</code>

<a name="options-data-shared">
</a>

#### data.shared : <code>object</code>

Root instance data: the GlobalMixin will inject reference to this object to all child Vue instance.::: tip INFOSee also [GlobalMixin](../../mixins/GlobalMixin) documentation.:::

<a name="options-data-shared-globals">
</a>

##### shared.GLOBALS : <code>object</code>

App component

**Default**: <code>Core.config.globals</code>  
<a name="options-data-usersettings">
</a>

#### data.userSettings : <code>object</code>

User settings object

<a name="options-beforecreate">
</a>

### Core.context.vue.root.options.beforeCreate() : <code>function</code>

Vue root instance beforeCreate hook.All `Core.context.vue.root.prototype.app` object properties will be assigned to `this._app` as read-only.See also: how to customize root instance hooks documentation

<a name="options-mounted">
</a>

### Core.context.vue.root.options.mounted() : <code>function</code>

Vue root instance mounted hookSee also: how to customize root instance hooks documentation

