---
title: Commons
headline: Commons
sidebarTitle: Commons
prev: ../
next: ./Page
---

# Commons

<a name="commons">
</a>

## commons : <code>object</code>

Each common component is automatically registered as global component.

