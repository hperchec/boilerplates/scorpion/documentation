---
title: VariantableMixin
headline: VariantableMixin
---

# VariantableMixin

<a name="variantablemixin">
</a>

## VariantableMixin

**Variantable** mixinMake a component 'variantable': it means that the component can be stylized with variantsThis feature is similar to [vue-tailwind](https://www.vue-tailwind.com/) components

