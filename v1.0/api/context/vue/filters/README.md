---
title: Filters
headline: Filters
sidebarTitle: Filters
prev: ../directives/
next: ../mixins/
---

# Filters

<a name="filters">
</a>

## filters : <code>object</code>

Contains all Vue filters that will be automatically registered.Each filter is also available in `vm.$filters` for each Vue instance.

**Schema**:

- [filters](#filters) : <code>object</code>
  - [.camelcase](#filters-camelcase)
  - [.capitalize](#filters-capitalize)
  - [.constantcase](#filters-constantcase)
  - [.dateFormat](#filters-dateformat)
  - [.kebabcase](#filters-kebabcase)
  - [.lowercase](#filters-lowercase)
  - [.snakecase](#filters-snakecase)
  - [.uppercase](#filters-uppercase)

<a name="filters-camelcase">
</a>

### Core.context.vue.filters.camelcase

Same as `camelcase` util::: tip See also[camelcase](../../utils/camelcase) util documentation:::

**Example**

```js
// Vue filter{{ 'foo bar' | camelcase }}// In Vue componentthis.$filters.camelcase(...)
```

<a name="filters-capitalize">
</a>

### Core.context.vue.filters.capitalize

Same as `capitalize` util::: tip See also[capitalize](../../utils/capitalize) util documentation:::

**Example**

```js
// Vue filter{{ 'foo bar' | capitalize }}// In Vue componentthis.$filters.capitalize(...)
```

<a name="filters-constantcase">
</a>

### Core.context.vue.filters.constantcase

Same as `constantcase` util::: tip See also[constantcase](../../utils/constantcase) util documentation:::

**Example**

```js
// Vue filter{{ 'foo bar' | constantcase }}// In Vue componentthis.$filters.constantcase(...)
```

<a name="filters-dateformat">
</a>

### Core.context.vue.filters.dateFormat

Same as `dateFormat` util::: tip See also[dateFormat](../../utils/date-format) util documentation:::

**Example**

```js
// Vue filter{{ new Date() | dateFormat(...) }}// In Vue componentthis.$filters.dateFormat(...)
```

<a name="filters-kebabcase">
</a>

### Core.context.vue.filters.kebabcase

Same as `kebabcase` util::: tip See also[kebabcase](../../utils/kebabcase) util documentation:::

**Example**

```js
// Vue filter{{ 'foo bar' | kebabcase }}// In Vue componentthis.$filters.kebabcase(...)
```

<a name="filters-lowercase">
</a>

### Core.context.vue.filters.lowercase

Same as `lowercase` util::: tip See also[lowercase](../../utils/lowercase) util documentation:::

**Example**

```js
// Vue filter{{ 'foo bar' | lowercase }}// In Vue componentthis.$filters.lowercase(...)
```

<a name="filters-snakecase">
</a>

### Core.context.vue.filters.snakecase

Same as `snakecase` util::: tip See also[snakecase](../../utils/snakecase) util documentation:::

**Example**

```js
// Vue filter{{ 'foo bar' | snakecase }}// In Vue componentthis.$filters.snakecase(...)
```

<a name="filters-uppercase">
</a>

### Core.context.vue.filters.uppercase

Same as `uppercase` util::: tip See also[uppercase](../../utils/uppercase) util documentation:::

**Example**

```js
// Vue filter{{ 'foo bar' | uppercase }}// In Vue componentthis.$filters.uppercase(...)
```

