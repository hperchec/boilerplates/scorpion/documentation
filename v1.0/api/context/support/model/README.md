---
title: Support/model
headline: Support/model
sidebarTitle: model
prev: ../
next: ./Model
---

# Support/model

<a name="models">
</a>

## .models : <code>object</code>

> [context](./).[models](#)

<a name="models-model">
</a>

### Core.context.support.model.Model : <code>function</code>

**See**: [Model](./Model)

