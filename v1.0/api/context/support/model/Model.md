---
title: Model class
headline: Model class
sidebarTitle: Model class
sidebarDepth: 0
prev: ./
next: false
---

# Model class

<a name="model">
</a>

## Model

> [context](../).[models](./).[Model](#)Model classEvery resource of your project should have a model representationextending this class to easily manage it.

**Schema**:

- [Model](#model)
  - [new Model(data)](#newmodelnew)
  - _instance_
    - [.defineAttribute(name, value, type, nullable)](#model-defineattribute) ⇒ <code>void</code>
  - _static_
    - [.toModelAttributeName(value)](#model-tomodelattributename) ⇒ <code>string</code>
    - [.toDataPropertyName(value)](#model-todatapropertyname) ⇒ <code>string</code>
    - [.generateAttributeGetter(name)](#model-generateattributegetter) ⇒ <code>function</code>
    - [.generateAttributeSetter(name, type, [nullable])](#model-generateattributesetter) ⇒ <code>function</code>
    - [.findByPK(key, collection)](#model-findbypk) ⇒ <code>\*</code>
    - [.collect([items])](#model-collect) ⇒ <code>Collection</code>

<a name="newmodelnew">
</a>

### new Model(data)

Create a Model

| Param | Type | Description |
| --- | --- | --- |
| data | <code>object</code> | Data object (data is received as { key: value, ... } object where key is in snake_case format) |

<a name="model-defineattribute">
</a>

### core.context.support.model.Model.defineAttribute(name, value, type, nullable) ⇒ <code>void</code>

Set attribute and its accessor and mutator (default creates a "_private_" proverty with same name prefixed by '_' (underscore))

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The attribute name |
| value | <code>\*</code> | The value |
| type | <code>\*</code> | The type |
| nullable | <code>boolean</code> | If the attribute is nullable |

<a name="model-tomodelattributename">
</a>

### Model.toModelAttributeName(value) ⇒ <code>string</code>

Function used to transform data attribute name to model attribute name (default converts to camelcase)

**Returns**: <code>string</code> - Returns the camelcased string

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| value | <code>string</code> | The data attribute name |

<a name="model-todatapropertyname">
</a>

### Model.toDataPropertyName(value) ⇒ <code>string</code>

Function used to transform model attribute name to data property name (default converts to snakecase)

**Returns**: <code>string</code> - Returns the snakecased string

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| value | <code>string</code> | The model attribute name |

<a name="model-generateattributegetter">
</a>

### Model.generateAttributeGetter(name) ⇒ <code>function</code>

Generate default attribute getter

**Returns**: <code>function</code> - Returns generated getter

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The attribute name |

<a name="model-generateattributesetter">
</a>

### Model.generateAttributeSetter(name, type, [nullable]) ⇒ <code>function</code>

Generate default attribute setter

**Returns**: <code>function</code> - Returns generated setter

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>string</code> |  | The attribute name |
| type | <code>string</code> |  | The type |
| [nullable] | <code>boolean</code> | <code>false</code> | Defines if attribute is nullable |

<a name="model-findbypk">
</a>

### Model.findByPK(key, collection) ⇒ <code>\*</code>

Find by primary key default function.This function will be called to find an instancefor example in a model collection

**Returns**: <code>\*</code> - Returns the found collection item

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| key | <code>\*</code> | The key |
| collection | <code>Collection</code> \| <code>Array</code> | The collection / array |

<a name="model-collect">
</a>

### Model.collect([items]) ⇒ <code>Collection</code>

Return a collection of instance of the model class

**Returns**: <code>Collection</code> - Returns a model collection

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [items] | <code>Array</code> | <code>[]</code> | The items |

