---
title: Support
headline: Support
sidebarTitle: Support
initialOpenGroupIndex: -1
prev: ../
next: ./collection/
---

# Support

<a name="support">
</a>

## support : <code>object</code>

Context shared classes / functions / utilities...

**Schema**:

- [support](#support) : <code>object</code>
  - [.collection](#support-collection) : <code>object</code>
  - [.model](#support-model) : <code>object</code>

<a name="support-collection">
</a>

### Core.context.support.collection : <code>object</code>

**See**: [collection](./collection)

<a name="support-model">
</a>

### Core.context.support.model : <code>object</code>

**See**: [model](./model)

