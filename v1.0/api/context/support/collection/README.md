---
title: Support/collection
headline: Support/collection
sidebarTitle: collection
prev: ../
next: ./Collection
---

# Support/collection

<a name="collection">
</a>

## collection : <code>object</code>

> **Ref.** : [Core](../../Core).[support](../).[collection](./)

<a name="collection-collection">
</a>

### Core.context.support.collection.Collection : <code>object</code>

**See**: [Collection](./Collection)

