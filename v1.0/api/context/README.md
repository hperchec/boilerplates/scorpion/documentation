---
title: Core context
headline: Core context
sidebarTitle: Context
initialOpenGroupIndex: -1
prev: ../config/
next: ./support/
---

# Core context

<a name="context">
</a>

## context : <code>object</code>

> **Ref.** : [Core](../Core).[context](./)This is the Core context. The context object contains the app specificities.

**Schema**:

- [context](#context) : <code>object</code>
  - [.models](#context-models) : <code>object</code>
  - [.services](#context-services) : <code>Proxy</code>
  - [.support](#context-support) : <code>object</code>
  - [.vue](#context-vue) : <code>object</code>

<a name="context-models">
</a>

### Core.context.models : <code>object</code>

Contains the app data models (e.g. "User").

**Default**: <code>{}</code>  
<a name="context-services">
</a>

### Core.context.services : <code>Proxy</code>

Contains the context services.

**Default**: <code>{}</code>  
**Read only**: true  
**See**: [services](../services)

<a name="context-support">
</a>

### Core.context.support : <code>object</code>

Contains the shared classes, functions, utilities...

**See**: [support](./support)

<a name="context-vue">
</a>

### Core.context.vue : <code>object</code>

Contains all necessary Vue options, methods, mixins, etc...

**See**: [vue](./vue)

