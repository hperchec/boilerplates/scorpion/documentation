---
title: Template tools - utils
headline: Template tools - utils
sidebarDepth: 0
prev: false
next: false
---

# Template tools - utils

<a name="utils">
</a>

## .utils : <code>object</code>

> [toolbox](../).[template](./).[utils](#)

<a name="utils-extend">
</a>

### utils.extend(_Core, path, value) ⇒ <code>void</code>

Extend Core

| Param | Type | Description |
| --- | --- | --- |
| _Core | <code>function</code> | The Core |
| path | <code>Array</code> \| <code>string</code> | The path of the property to merge |
| value | <code>\*</code> | The value |

**Example**

```js
// In template extend:extend(Core, 'router.routes.public', publicRoutes)
```

