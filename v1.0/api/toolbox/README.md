---
title: Toolbox
headline: Toolbox
sidebarTitle: Toolbox
initialOpenGroupIndex: -1
prev: ../services/
next: ./services/
---

# Toolbox

<a name="toolbox">
</a>

## .toolbox : <code>object</code>

ScorpionUI toolbox.```jsimport { toolbox } from '@hperchec/scorpion-ui'```

**Schema**:

- [.toolbox](#toolbox) : <code>object</code>
  - [.Hook](#toolbox-hook) : <code>function</code>
  - [.services](#toolbox-services) : <code>object</code>
  - [.template](#toolbox-template) : <code>object</code>
  - [.vue](#toolbox-vue) : <code>object</code>

<a name="toolbox-hook">
</a>

### toolbox.Hook : <code>function</code>

**See**: [Hook](./Hook)

<a name="toolbox-services">
</a>

### toolbox.services : <code>object</code>

**See**: [services](./services)

<a name="toolbox-template">
</a>

### toolbox.template : <code>object</code>

**See**: [template](./template)

<a name="toolbox-vue">
</a>

### toolbox.vue : <code>object</code>

**See**: [vue](./vue)

