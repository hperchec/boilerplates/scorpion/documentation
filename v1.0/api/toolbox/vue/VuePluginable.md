---
title: VuePluginable class
headline: VuePluginable class
sidebarTitle: .VuePluginable
sidebarDepth: 0
prev: false
next: false
---

# VuePluginable class

<a name="vuepluginable">
</a>

## VuePluginable

> [context](../).[services](./).[VuePluginable](#)Abstract class that defines an object as a Vue plugin

**Schema**:

- [VuePluginable](#vuepluginable)
  - [new VuePluginable([data])](#newvuepluginablenew)
  - _instance_
    - _methods_
      - [.setVMProp(name, value)](#vuepluginable-setvmprop) ⇒ <code>void</code>
      - [._initVM(data)](#vuepluginable-initvm)
      - [.destroyVM()](#vuepluginable-destroyvm) ⇒ <code>void</code>
    - _properties_
      - [.vm](#vuepluginable-vm) : <code>Vue</code>
  - _static_
    - [.install(_Vue, [options])](#vuepluginable-install)
    - _methods_
      - [.createMixin()](#vuepluginable-createmixin) ⇒ <code>object</code>
    - _properties_
      - [.reactive](#vuepluginable-reactive) : <code>boolean</code>
      - [.rootOption](#vuepluginable-rootoption) : <code>string</code>
      - [.aliases](#vuepluginable-aliases) : <code>Array.&lt;string&gt;</code>

<a name="newvuepluginablenew">
</a>

### new VuePluginable([data])

VuePluginable abstract class cannot be instantiated with 'new'

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [data] | <code>object</code> | <code>{}</code> | The data to send to _initVM method for reactivity (if reactive = true) |

<a name="vuepluginable-setvmprop">
</a>

### vuePluginable.setVMProp(name, value) ⇒ <code>void</code>

Set a property to this._vm

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | Name of the property |
| value | <code>\*</code> | The value |

<a name="vuepluginable-initvm">
</a>

### vuePluginable.\_initVM(data)

Init the view model to use with Vue

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| data | <code>object</code> | The constructor data |

<a name="vuepluginable-destroyvm">
</a>

### vuePluginable.destroyVM() ⇒ <code>void</code>

Destroy the vm

**Category**: methods  
<a name="vuepluginable-vm">
</a>

### vuePluginable.vm : <code>Vue</code>

vm

**Category**: properties  
**Read only**: true  
<a name="vuepluginable-install">
</a>

### VuePluginable.install(_Vue, [options])

Install method for Vue plugin

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | The Vue instance |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

<a name="vuepluginable-createmixin">
</a>

### VuePluginable.createMixin() ⇒ <code>object</code>

Create a mixin to apply to Vue

**Returns**: <code>object</code> - Return the created mixin

**Category**: methods  
<a name="vuepluginable-reactive">
</a>

### VuePluginable.reactive : <code>boolean</code>

Static property 'reactive': defines if the service is reactive (will create a Vue instance to make data reactive)

**Default**: <code>false</code>  
**Category**: properties  
<a name="vuepluginable-rootoption">
</a>

### VuePluginable.rootOption : <code>string</code>

Static property 'rootOption': defines if the service must be passed as Vue root instance option

**Default**: <code>&quot;undefined&quot;</code>  
**Category**: properties  
<a name="vuepluginable-aliases">
</a>

### VuePluginable.aliases : <code>Array.&lt;string&gt;</code>

Static property 'aliases'

**Default**: <code>[]</code>  
**Category**: properties  
