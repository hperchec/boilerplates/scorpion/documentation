---
title: Route class
headline: Route class
sidebarTitle: .Route
sidebarDepth: 0
prev: false
next: false
---

# Route class

<a name="route">
</a>

## Route

> [context](../).[router](./).[Route](#)Class that represents a route (view)

**Schema**:

- [Route](#route)
  - [new Route(path, name, component, [options])](#newroutenew)
  - _methods_
    - [.setMeta(meta)](#route-setmeta) ⇒ <code>void</code>
  - _properties_
    - [.defaultMeta](#route-defaultmeta) : <code>object</code>
    - [.path](#route-path) : <code>string</code>
    - [.name](#route-name) : <code>string</code>
    - [.component](#route-component) : <code>object</code> \| <code>null</code>
    - [.alias](#route-alias) : <code>string</code>
    - [.redirect](#route-redirect) : <code>string</code> \| <code>object</code> \| <code>function</code>
    - [.children](#route-children) : <code>Array.&lt;object&gt;</code>
    - [.props](#route-props) : <code>boolean</code> \| <code>object</code> \| <code>function</code>
    - [.beforeEnter](#route-beforeenter) : <code>function</code>
    - [.meta](#route-meta) : <code>object</code>

<a name="newroutenew">
</a>

### new Route(path, name, component, [options])

Create a Route

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| path | <code>string</code> |  | The route path |
| name | <code>string</code> |  | The route name |
| component | <code>function</code> \| <code>null</code> |  | A function that returns the route Vue component (can be null) |
| [options] | <code>object</code> | <code>{}</code> | Object that contains other the vue-router route object properties |
| [options.alias] | <code>string</code> |  | The route alias |
| [options.redirect] | <code>string</code> \| <code>object</code> \| <code>function</code> |  | The route redirect |
| [options.children] | <code>Array.&lt;object&gt;</code> |  | The route children |
| [options.props] | <code>boolean</code> \| <code>object</code> \| <code>function</code> |  | The route props |
| [options.beforeEnter] | <code>function</code> |  | The route beforeEnter function |
| [options.meta] | <code>object</code> |  | The route meta fields |

<a name="route-setmeta">
</a>

### route.setMeta(meta) ⇒ <code>void</code>

Set meta fields

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| meta | <code>object</code> | The meta object to assign |

<a name="route-defaultmeta">
</a>

### route.defaultMeta : <code>object</code>

Default meta

**Category**: properties  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| requiresAuth | <code>boolean</code> | If route requires authentication (default: `false`) |
| layout | <code>object</code> | The page layout (vue component) (default: `undefined`) |
| requiresVerifiedEmail | <code>boolean</code> | If route requires a verified email (default: `false`) |
| breadcrumb | <code>object</code> | Breadcrumb options. See also: |
| pageMeta | <code>object</code> | Page meta |
| pageMeta.title | <code>string</code> | Page title |

<a name="route-path">
</a>

### route.path : <code>string</code>

The route path (See also: [vue-router documentation](https://router.vuejs.org/api/#path))

**Category**: properties  
<a name="route-name">
</a>

### route.name : <code>string</code>

The route name (See also: [vue-router documentation](https://router.vuejs.org/api/#name))

**Category**: properties  
<a name="route-component">
</a>

### route.component : <code>object</code> \| <code>null</code>

The route component (See also: [vue-router documentation](https://router.vuejs.org/guide/essentials/dynamic-matching.html))

**Category**: properties  
<a name="route-alias">
</a>

### route.alias : <code>string</code>

The route alias (See also: [vue-router documentation](https://router.vuejs.org/api/#alias))

**Category**: properties  
<a name="route-redirect">
</a>

### route.redirect : <code>string</code> \| <code>object</code> \| <code>function</code>

The route redirect (See also: [vue-router documentation](https://router.vuejs.org/guide/essentials/redirect-and-alias.html#redirect))

**Category**: properties  
<a name="route-children">
</a>

### route.children : <code>Array.&lt;object&gt;</code>

The route children (See also: [vue-router documentation](https://router.vuejs.org/api/#children))

**Category**: properties  
<a name="route-props">
</a>

### route.props : <code>boolean</code> \| <code>object</code> \| <code>function</code>

The route props (See also: [vue-router documentation](https://router.vuejs.org/api/#props))

**Category**: properties  
<a name="route-beforeenter">
</a>

### route.beforeEnter : <code>function</code>

The route beforeEnter function (See also: [vue-router documentation](https://router.vuejs.org/api/#beforeenter))

**Category**: properties  
<a name="route-meta">
</a>

### route.meta : <code>object</code>

The route meta fields (See also: [vue-router documentation](https://router.vuejs.org/api/#meta))

**Category**: properties  
**Read only**: true  
