---
title: Service (router) options
headline: Service (router) options
sidebarTitle: Options
sidebarDepth: 0
prev: false
next: false
---

# Service (router) options

<a name="options">
</a>

## .options

> [context](../).[router](./).[options](#)Accepts all VueRouter options plus the following:- `beforeEach`- `beforeResolve`- `afterEach`- `logger`- `log`- `translator`- `translate`- `catchAllRedirect`By default, each **private** and **public** route are auto-injected as `routes` option.Each object in `Core.context.services['router'].routes.private` and`Core.context.services['router'].routes.public` will be passed respectively to `PrivateRoute` and `PublicRoute` constructor.The result will be an array of the defined routes. You can overwrite it by setting this option.Configurable options:- **catchAllRedirect**: `Core.config.services['router'].catchAllRedirect`::: tip SEE ALSO[VueRouter construction options documentation](https://v3.router.vuejs.org/api/#router-construction-options):::

**Schema**:

- [.options](#options)
  - [.beforeEach](#options-beforeeach) : <code>Array.&lt;function()&gt;</code>
  - [.beforeResolve](#options-beforeresolve) : <code>Array.&lt;function()&gt;</code>
  - [.afterEach](#options-aftereach) : <code>Array.&lt;function()&gt;</code>
  - [.catchAllRedirect](#options-catchallredirect) : <code>string</code> \| <code>object</code>
  - [.logger()](#options-logger) ⇒ <code>\*</code>
  - [.log(...args)](#options-log) ⇒ <code>\*</code>
  - [.translator()](#options-translator) ⇒ <code>\*</code>
  - [.translate(...args)](#options-translate) ⇒ <code>\*</code>

<a name="options-beforeeach">
</a>

### options.beforeEach : <code>Array.&lt;function()&gt;</code>

Global before hook. Each function will be injected during router service creationvia `router.beforeEach()` before returning new router instance.::: tip SEE ALSO[VueRouter instance beforeEach documentation](https://v3.router.vuejs.org/api/#router-beforeeach):::By default, it is a empty `XArray` (see utils documentation). Each middleware in `Core.context.services['router'].middlewares`will be injected at service creation.The result will be a concatenated array of the defined middlewares and the `beforeEach` option array content.

<a name="options-beforeresolve">
</a>

### options.beforeResolve : <code>Array.&lt;function()&gt;</code>

Global before resolve hook. Each function will be injected during router service creationvia `router.beforeResolve()` before returning new router instance.::: tip SEE ALSO[VueRouter instance beforeResolve documentation](https://v3.router.vuejs.org/api/#router-beforeresolve):::By default, it is an empty `XArray`

<a name="options-aftereach">
</a>

### options.afterEach : <code>Array.&lt;function()&gt;</code>

Global after hook. Each function will be injected during router service creationvia `router.afterEach()` before returning new router instance.::: tip SEE ALSO[VueRouter instance afterEach documentation](https://v3.router.vuejs.org/api/#router-aftereach):::By default, it is an empty `XArray`

<a name="options-catchallredirect">
</a>

### options.catchAllRedirect : <code>string</code> \| <code>object</code>

The path / route object to redirect in _CatchAll before hook.Can be overwritten via Core global configuration: `Core.config.services['router'].catchAllRedirect`

**Default**: <code>&quot;/404&quot;</code>  
<a name="options-logger">
</a>

### options.logger() ⇒ <code>\*</code>

Logger. Default is 'logger' service

**Returns**: <code>\*</code> - Logger instance/object

<a name="options-log">
</a>

### options.log(...args) ⇒ <code>\*</code>

Function to log in console. Default is 'logger' service 'consoleLog' method

**Returns**: <code>\*</code> - Default returns nothing

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same arguments as service method |

<a name="options-translator">
</a>

### options.translator() ⇒ <code>\*</code>

Translator. Default is 'i18n' service

**Returns**: <code>\*</code> - Translator instance/object

<a name="options-translate">
</a>

### options.translate(...args) ⇒ <code>\*</code>

Function to translate string. Default is 'i18n' service 't' method

**Returns**: <code>\*</code> - Default returns string

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same arguments as service method |

