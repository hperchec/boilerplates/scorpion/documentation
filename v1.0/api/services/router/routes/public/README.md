---
title: Router - 'public' routes
headline: Router - 'public' routes
sidebarTitle: .public
sidebarDepth: 0
prev: false
next: false
---

# Router - &#x27;public&#x27; routes

<a name="public">
</a>

## .public : <code>object</code>

> [context](../../).[router](../).[routes](./).[public](#)

**Schema**:

- [.public](#public) : <code>object</code>
  - [.Home](#public-home) : <code>object</code>
  - [._CatchAll](#public-catchall) : <code>object</code>
  - [.NotFound](#public-notfound) : <code>object</code>

<a name="public-home">
</a>

### public.Home : <code>object</code>

Home route

**See**: [Home](./Home)

<a name="public-catchall">
</a>

### public.\_CatchAll : <code>object</code>

"Catch all" route

**See**: [_CatchAll](./_CatchAll)

<a name="public-notfound">
</a>

### public.NotFound : <code>object</code>

NotFound route

**See**: [NotFound](./NotFound)

