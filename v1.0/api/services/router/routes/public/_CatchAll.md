---
title: CatchAll route
headline: CatchAll route
sidebarTitle: ._CatchAll
sidebarDepth: 0
prev: false
next: false
---

# CatchAll route

<a name="catchall">
</a>

## .\_CatchAll : <code>object</code>

> [context](../../../).[router](../../).[routes](../).[public](./).[_CatchAll](#)- **Name**: _CatchAll- **Path**: `*`- **Params**: *none*- **Query**: *none*

**Schema**:

- [._CatchAll](#catchall) : <code>object</code>
  - [.path](#catchall-path) : <code>string</code>
  - [.name](#catchall-name) : <code>string</code>
  - [.component](#catchall-component) : <code>null</code>

<a name="catchall-path">
</a>

### _CatchAll.path : <code>string</code>

Route path

**Default**: <code>&quot;&#x27;*&#x27;&quot;</code>  
<a name="catchall-name">
</a>

### _CatchAll.name : <code>string</code>

Route name

**Default**: <code>&quot;_CatchAll&quot;</code>  
<a name="catchall-component">
</a>

### _CatchAll.component : <code>null</code>

Route component

**Default**: <code>null</code>  
