---
title: Router - 'private' routes
headline: Router - 'private' routes
sidebarTitle: .private
sidebarDepth: 0
prev: false
next: false
---

# Router - &#x27;private&#x27; routes

<a name="private">
</a>

## .private : <code>object</code>

> [context](../../).[router](../).[routes](./).[private](#)

