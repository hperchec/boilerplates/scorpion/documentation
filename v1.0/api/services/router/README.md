---
title: Service (router)
headline: Service (router)
sidebarTitle: router
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (router)

<a name="service"></a>

The service "router" is for routing.It uses the [vue-router](https://router.vuejs.org/) package.

## Register

On registering, the service will define Vue components:- `Core.context.vue.components.public.views`: see [public components](./components/public/)- `Core.context.vue.components.private.views`: see [private components](./components/private/)It will also define Vue mixin:- `Core.context.vue.mixins.RouteComponentMixin`: see [RouteComponentMixin](./mixins/RouteComponentMixin)If the service "logger" is registered too, it will define a `Logger` [type](../logger/Logger#types) as following:```jsrouter: {  badgeContent: '[ROUTER]',  badgeColor: '#AFD3E4',  badgeBgColor: '#3883A8',  messageColor: '#1C4254',  prependMessage: '🚦'}```

## Create

Returns an instance of `VueRouter` created with options.

**Returns**: <code>VueRouter</code> - Once services are created:

```js
Core.service('router')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['router']`.

Plugin will be automatically used by Vue.

::: tip SEE ALSO[vue-router](https://router.vuejs.org/) documentation:::

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

## Expose (service context)

The service will expose the following properties under `Core.context.services['router']`:
<a name="expose-middlewares">
</a>

### .middlewares

> Exposed as `Core.context.services['router'].middlewares`

**See**: [middlewares](./middlewares)

<a name="expose-routes">
</a>

### .routes

> Exposed as `Core.context.services['router'].routes`

**See**: [routes](./routes)

<a name="expose-route">
</a>

### .Route

> Exposed as `Core.context.services['router'].Route`

**See**: [Route](./Route)

<a name="expose-privateroute">
</a>

### .PrivateRoute

> Exposed as `Core.context.services['router'].PrivateRoute`

**See**: [PrivateRoute](./PrivateRoute)

<a name="expose-publicroute">
</a>

### .PublicRoute

> Exposed as `Core.context.services['router'].PublicRoute`

**See**: [PublicRoute](./PublicRoute)

