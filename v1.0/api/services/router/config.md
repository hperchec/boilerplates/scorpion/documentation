---
title: Service (router) configuration
headline: Service (router) configuration
sidebarTitle: Configuration
prev: false
next: false
---

# Service (router) configuration

<a name="configuration">
</a>

## Configuration

Service configuration> Refers to `Core.config.services['router']````js'router': {  // The route to redirect in _CatchAll route before hook.  catchAllRedirect: String|Object}```

<a name="configuration-catchallredirect">
</a>

### Core.config.services[&#x27;router&#x27;].catchAllRedirect : <code>string</code> \| <code>object</code>

The route to redirect in _CatchAll route before hook.

