# NotFoundRouteComponent

NotFound: not found (404) page component

> This component is globally registered by Vue

**Mixins**:
- RouteComponent: `..\..\..\..\mixins\RouteComponentMixin.js`

