# HomeRouteComponent

Home: home page component

> This component is globally registered by Vue

**See**:
- [toto.com](toto.com)

**Authors**:
- Hervé Perchec

**Mixins**:
- RouteComponent: `..\..\..\..\mixins\RouteComponentMixin.js`

