---
title: Router - AppMiddleware
headline: Router - AppMiddleware
sidebarTitle: .AppMiddleware
sidebarDepth: 0
prev: false
next: false
---

# Router - AppMiddleware

<a name="appmiddleware">
</a>

## .AppMiddleware ⇒ <code>void</code>

> [context](../../../).[router](../../).[middlewares](../).[AppMiddleware](./)See also [vue-router beforeEnter guard documentation](https://v3.router.vuejs.org/guide/advanced/navigation-guards.html#global-before-guards)App 'root' middleware.Fill route `meta.from` with the `from` route object

