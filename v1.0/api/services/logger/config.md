---
title: Service (logger) configuration
headline: Service (logger) configuration
sidebarTitle: Configuration
prev: false
next: false
---

# Service (logger) configuration

<a name="configuration">
</a>

## Configuration

Service configuration> Refers to `Core.config.services['logger']````js'logger': {  // Enable/disable logger  log: Boolean,  // Logger types  types: Object}```

**Schema**:

- [Configuration](#configuration)
  - [.log](#configuration-log) : <code>boolean</code>
  - [.types](#configuration-types) : <code>object</code>

<a name="configuration-log">
</a>

### Core.config.services[&#x27;logger&#x27;].log : <code>boolean</code>

Enable/Disable logger.

<a name="configuration-types">
</a>

### Core.config.services[&#x27;logger&#x27;].types : <code>object</code>

Define types for Logger (see Logger constructor options).

