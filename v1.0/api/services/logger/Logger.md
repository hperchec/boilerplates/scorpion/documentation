---
title: Logger
headline: Logger
---

# Logger

## Classes

<dl>
<dt><a href="#Logger">Logger</a> ⇐ <code>VuePluginable</code></dt>
<dd><blockquote>
<p><a href="../../">context</a>.<a href="../">services</a>[&#39;<a href="./">logger</a>&#39;].<a href="#">Logger</a></p>
</blockquote>
<p>Service <strong>logger</strong>: Logger class</p>
</dd>
</dl>

## Members

<dl>
<dt><a href="#log">log</a> : <code>boolean</code></dt>
<dd><p>Enable the logger</p>
</dd>
<dt><a href="#types">types</a> : <code>object</code></dt>
<dd><p>Log types</p>
</dd>
</dl>

<a name="logger">
</a>

## Logger ⇐ <code>VuePluginable</code>

> [context](../../).[services](../)['[logger](./)'].[Logger](#)Service **logger**: Logger class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [Logger](#logger) ⇐ <code>VuePluginable</code>
  - [new Logger([options])](#newloggernew)
  - _instance_
    - [.rootOption](#logger-rootoption)
    - [.defaultOptions](#logger-defaultoptions)
    - [.setType(name, [options])](#logger-settype) ⇒ <code>void</code>
    - _methods_
      - [.consoleLog(message, [options])](#logger-consolelog) ⇒ <code>void</code>
    - _property_
      - [.log](#logger-log) : <code>boolean</code>
      - [.types](#logger-types) : <code>object</code>
  - _static_
    - [.install(_Vue, [options])](#logger-install)

<a name="newloggernew">
</a>

### new Logger([options])

Create a new instance

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [options] | <code>object</code> | <code>{}</code> | The Logger options |
| [options.log] | <code>boolean</code> |  | Enable/disable logger |
| [options.types] | <code>object</code> |  | Logger types |

<a name="logger-rootoption">
</a>

### logger.rootOption

Overrides static VuePluginable properties

<a name="logger-defaultoptions">
</a>

### logger.defaultOptions

Default options

<a name="logger-settype">
</a>

### logger.setType(name, [options]) ⇒ <code>void</code>

setType

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>string</code> |  | The type name to add/overwrite |
| [options] | <code>object</code> | <code>{}</code> | The type object |
| [options.badgeContent] | <code>string</code> |  | The badge content (Default: `default` type value) |
| [options.badgeColor] | <code>string</code> |  | The badge text color (Default: `default` type value) |
| [options.badgeBgColor] | <code>string</code> |  | The badge background color (Default: `default` type value) |
| [options.timeColor] | <code>string</code> |  | The displayed time text color (Default: `default` type value) |
| [options.messageColor] | <code>string</code> |  | The message text color (Default: `default` type value) |
| [options.prependMessage] | <code>string</code> |  | The string to prepend to message (Default: `default` type value) |

**Example**

```js
logger.setType('info', {  badgeContent: '[INFO]',  badgeColor: '#FDE5DC',  badgeBgColor: '#F58E69',  timeColor: '#551919',  messageColor: '#A4330B',  prependMessage: '❔'})
```

<a name="logger-consolelog">
</a>

### logger.consoleLog(message, [options]) ⇒ <code>void</code>

Log a message in the console

**Category**: methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| message | <code>string</code> |  | The message |
| [options] | <code>object</code> |  | The options |
| [options.type] | <code>string</code> | <code>&quot;&#x27;default&#x27;&quot;</code> | Log type ('default', 'warning', 'error', ...) |
| [options.dev] | <code>boolean</code> | <code>true</code> | Define if log must be displayed in development mode |
| [options.prod] | <code>boolean</code> | <code>true</code> | Define if log must be displayed in production mode |

**Example**

```js
logger.consoleLog('Development mode enabled', { prod: false })logger.consoleLog('Production mode enabled', { type: 'info', dev: false })logger.consoleLog('Use "const" or "let" instead of "var"!', { type: 'advice' })logger.consoleLog('Parameter "options" is missing. Set to default...', { type: 'warning' })logger.consoleLog('Server is not responding...', { type: 'error' })logger.consoleLog('App booted!', { type: 'system' })logger.consoleLog('Redirecting...', { type: 'router' })logger.consoleLog('State mutated!', { type: 'store' })
```

<a name="logger-log">
</a>

### logger.log : <code>boolean</code>

log

**Category**: property  
**Read only**: true  
<a name="logger-types">
</a>

### logger.types : <code>object</code>

types

**Category**: property  
**Read only**: true  
<a name="logger-install">
</a>

### Logger.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

<a name="log">
</a>

## log : <code>boolean</code>

Enable the logger

<a name="types">
</a>

## types : <code>object</code>

Log types

