---
title: Service (logger) options
headline: Service (logger) options
sidebarTitle: .options
sidebarDepth: 0
prev: false
next: false
---

# Service (logger) options

<a name="options">
</a>

## .options

> [context](../).[logger](./).[options](#)Same as Logger constructor options.Configurable options:- `log`: `Core.config.services['logger'].log`- `types`: `Core.config.services['logger'].types`

