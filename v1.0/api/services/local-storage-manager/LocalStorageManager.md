---
title: LocalStorageManager
headline: LocalStorageManager
---

# LocalStorageManager

<a name="localstoragemanager">
</a>

## LocalStorageManager ⇐ <code>VuePluginable</code>

> [context](../../).[services](../)['[local-storage-manager](./)'].[LocalStorageManager](#)Service **local-storage-manager**: LocalStorageManager class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [LocalStorageManager](#localstoragemanager) ⇐ <code>VuePluginable</code>
  - [new LocalStorageManager([options])](#newlocalstoragemanagernew)
  - _instance_
    - [.originalSetItemMethod](#localstoragemanager-originalsetitemmethod)
    - [.getTypeFromLSValue(lsValue)](#localstoragemanager-gettypefromlsvalue) ⇒ <code>string</code>
    - [.hasItem(key)](#localstoragemanager-hasitem) ⇒ <code>boolean</code>
    - _properties_
      - [.castMethods](#localstoragemanager-castmethods) : <code>object</code>
      - [.setItemEventName](#localstoragemanager-setitemeventname) : <code>string</code>
      - [.removeItemEventName](#localstoragemanager-removeitemeventname) : <code>string</code>
      - [.items](#localstoragemanager-items) : <code>object</code>
      - [.schema](#localstoragemanager-schema) : <code>object</code>
    - _public methods_
      - [.onSetItem(func)](#localstoragemanager-onsetitem) ⇒ <code>void</code>
      - [.onRemoveItem(func)](#localstoragemanager-onremoveitem) ⇒ <code>void</code>
  - _static_
    - [.checkCompat()](#localstoragemanager-checkcompat) ⇒ <code>void</code>
    - [.init()](#localstoragemanager-init) ⇒ <code>void</code>
    - [.isKeyDefined(key)](#localstoragemanager-iskeydefined) ⇒ <code>boolean</code>
    - [.install(_Vue, [options])](#localstoragemanager-install)

<a name="newlocalstoragemanagernew">
</a>

### new LocalStorageManager([options])

Create a new LocalStorage manager

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [options] | <code>object</code> | <code>{}</code> | The constructor options |
| [options.schema] | <code>object</code> |  | The local storage items object schema (`{ [itemKey: string]: 'string' || 'number' || 'object' }`) |
| [options.setItemEventName] | <code>string</code> |  | The event name to dispatch at document level when item is set (default is: 'ls-item-set') |
| [options.removeItemEventName] | <code>string</code> |  | The event name to dispatch at document level when item is removed (default is: 'ls-item-removed') |
| [options.onSetItem] | <code>function</code> \| <code>Array.&lt;function()&gt;</code> |  | The callback function(s) to call when item is set (See also onSetItem method) |
| [options.onRemoveItem] | <code>function</code> \| <code>Array.&lt;function()&gt;</code> |  | The callback function(s) to call when item is removed (See also onRemoveItem method) |

<a name="localstoragemanager-originalsetitemmethod">
</a>

### localStorageManager.originalSetItemMethod

Public static properties

<a name="localstoragemanager-gettypefromlsvalue">
</a>

### localStorageManager.getTypeFromLSValue(lsValue) ⇒ <code>string</code>

Auto-detect type of local storage item value

**Returns**: <code>string</code> - Returns 'string', 'number' or 'object'

| Param | Type | Description |
| --- | --- | --- |
| lsValue | <code>string</code> | The local storage item value (string) |

**Example**

```js
localStorageManager.getTypeFromLSValue('example') // => 'string'localStorageManager.getTypeFromLSValue('42') // => 'number'localStorageManager.getTypeFromLSValue('{ "prop1": "test1", "prop2": "test2" }') // => 'object'
```

<a name="localstoragemanager-hasitem">
</a>

### localStorageManager.hasItem(key) ⇒ <code>boolean</code>

Check if provided item key is defined

**Returns**: <code>boolean</code> - Returns true if key is set, else false

| Param | Type | Description |
| --- | --- | --- |
| key | <code>string</code> | The item key |

<a name="localstoragemanager-castmethods">
</a>

### localStorageManager.castMethods : <code>object</code>

castMethods

**Category**: properties  
<a name="localstoragemanager-setitemeventname">
</a>

### localStorageManager.setItemEventName : <code>string</code>

setItemEventName

**Category**: properties  
<a name="localstoragemanager-removeitemeventname">
</a>

### localStorageManager.removeItemEventName : <code>string</code>

removeItemEventName

**Category**: properties  
<a name="localstoragemanager-items">
</a>

### localStorageManager.items : <code>object</code>

Local storage cache

**Category**: properties  
**Read only**: true  
<a name="localstoragemanager-schema">
</a>

### localStorageManager.schema : <code>object</code>

Local storage schema (key/type)

**Category**: properties  
**Read only**: true  
<a name="localstoragemanager-onsetitem">
</a>

### localStorageManager.onSetItem(func) ⇒ <code>void</code>

**SET_ITEM** hook append method.The function to append, takes three arguments:```tsfunction (key: String, cacheValue: any, lsValue: String) => void```- **key**: `{String}` the item key- **cacheValue**: `{any}` the cache value- **lsValue**: `{String}` the value in local storage

**Category**: public methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function(s) to append to the hook queue |

**Example**

```js
localStorageManager.onSetItem((key, cacheValue, lsValue) => {  console.log(`Set item "${key}":`, cacheValue)})
```

<a name="localstoragemanager-onremoveitem">
</a>

### localStorageManager.onRemoveItem(func) ⇒ <code>void</code>

**REMOVE_ITEM** hook append method.The function to append, takes one argument:```tsfunction (key: String) => void```- **key**: `{String}` the item key

**Category**: public methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> \| <code>Array.&lt;function()&gt;</code> | The function(s) to append to the hook queue |

**Example**

```js
localStorageManager.onRemoveItem((key) => {  console.log(`Remove item "${key}"`)})
```

<a name="localstoragemanager-checkcompat">
</a>

### LocalStorageManager.checkCompat() ⇒ <code>void</code>

Check compatibility. Throws error if localStorage variable is undefined

<a name="localstoragemanager-init">
</a>

### LocalStorageManager.init() ⇒ <code>void</code>

Initialize LocalStorageManager. Check compatibility + event listener to observe changes.

<a name="localstoragemanager-iskeydefined">
</a>

### LocalStorageManager.isKeyDefined(key) ⇒ <code>boolean</code>

Check if a localStorage key is defined

**Returns**: <code>boolean</code> - Returns true if key is defined, else false

| Param | Type | Description |
| --- | --- | --- |
| key | <code>string</code> | The key in local storage |

<a name="localstoragemanager-install">
</a>

### LocalStorageManager.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

