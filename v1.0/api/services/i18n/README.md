---
title: Service (i18n)
headline: Service (i18n)
sidebarTitle: i18n
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (i18n)

<a name="service"></a>

The service "i18n" is for internationalization / translations.It uses the [vue-i18n](https://kazupon.github.io/vue-i18n/) package.

## Register

On registering, the service will add mixin to rootinstance options (`Core.context.vue.root.options.mixins`)to share `LANGS` computed and define `locale` [user setting](/guide/user-settings).At created hook, `locale` user setting is automatically set.Service also injects properties to Vue root instance prototype `$app`:- `$app.setLocale`- `$app.currentLocale`

## Create

Under the ground, service uses `@cospired/i18n-iso-languages` library.For each `availableLocales` defined in the service **options**, the correspondinglanguage will be loaded (`@cospired/i18n-iso-languages/langs/<language>.json`).Returns an instance of `VueI18n` created with other options.

**Returns**: <code>VueI18n</code> - Once services are created:

```js
Core.service('i18n')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['i18n']`.

Plugin will be automatically used by Vue.

::: tip SEE ALSO[vue-i18n](https://kazupon.github.io/vue-i18n/) documentation:::

### Root instance

<a name="shared"></a>

#### Shared data

> By adding properties to \`data.shared\`, each component (\`vm\`) will have corresponding computed defined

::: tip See also
[GlobalMixin](../../context/vue/mixins/global/GlobalMixin) documentation
:::

/**

<a name="shared-langs">
</a>

##### vm.LANGS : <code>object</code>

The registered languages.Object like:```js{  en: { lang: 'en', name: 'English' },  fr: { lang: 'fr', name: 'Français' },  ...}```

**Example**

```js
// In any componentthis.LANGS
```

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

**Schema**:

- [$app](#app) : <code>object</code>
  - [.userSettings](#app-usersettings) : <code>object</code>
    - [.locale](#app-usersettings-locale) : <code>string</code>
  - [.setLocale(locale)](#app-setlocale) ⇒ <code>void</code>
  - [.currentLocale()](#app-currentlocale) ⇒ <code>string</code>

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

<a name="app-usersettings-locale">
</a>

###### vm.$app.userSettings.locale : <code>string</code>

The language (will be set in created hook). Default is `Core.config.services['i18n'].defaultLocale`

##### Others

<a name="app-setlocale">
</a>

###### vm.$app.setLocale(locale) ⇒ <code>void</code>

Set locale setting and its value in Local storage

| Param | Type | Description |
| --- | --- | --- |
| locale | <code>string</code> | Locale |

**Example**

```js
// In any componentthis.$app.setLocale('en')
```

<a name="app-currentlocale">
</a>

###### vm.$app.currentLocale() ⇒ <code>string</code>

Get current locale (from $i18n)

**Returns**: <code>string</code> - The current locale

**Example**

```js
this.$app.currentLocale()
```

## Expose (service context)

The service will expose the following properties under `Core.context.services['i18n']`:
<a name="expose-messages">
</a>

### .messages

> Exposed as `Core.context.services['i18n'].messages`

**See**: [messages](./messages)

