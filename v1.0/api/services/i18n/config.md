---
title: Service (i18n) configuration
headline: Service (i18n) configuration
sidebarTitle: Configuration
prev: false
next: false
---

# Service (i18n) configuration

<a name="configuration">
</a>

## Configuration

Service configuration> Refers to `Core.config.services['i18n']````js'i18n': {  // The available locales.  availableLocales: String[],  // Default locale. VueI18n `locale` option.  defaultLocale: String,  // Fallback locale. VueI18n `fallbackLocale` option.  fallbackLocale: String}```

**Schema**:

- [Configuration](#configuration)
  - [.availableLocales](#configuration-availablelocales) : <code>Array.&lt;string&gt;</code>
  - [.defaultLocale](#configuration-defaultlocale) : <code>string</code>
  - [.fallbackLocale](#configuration-fallbacklocale) : <code>string</code>

<a name="configuration-availablelocales">
</a>

### Core.config.services[&#x27;i18n&#x27;].availableLocales : <code>Array.&lt;string&gt;</code>

An array of available locales.

<a name="configuration-defaultlocale">
</a>

### Core.config.services[&#x27;i18n&#x27;].defaultLocale : <code>string</code>

The default locale.

<a name="configuration-fallbacklocale">
</a>

### Core.config.services[&#x27;i18n&#x27;].fallbackLocale : <code>string</code>

The fallback locale.

