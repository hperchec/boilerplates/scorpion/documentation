---
title: Internationalization (i18n) - messages
headline: Internationalization (i18n) - messages
sidebarTitle: .messages
sidebarDepth: 0
prev: false
next: false
---

# Internationalization (i18n) - messages

<a name="messages">
</a>

## .messages : <code>object</code>

> [context](../).[i18n](./).[messages](#)::: tip INFOPlease check the [internationalization](../../../../internationalization) documentation.:::

