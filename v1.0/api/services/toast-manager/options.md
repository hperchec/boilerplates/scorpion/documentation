---
title: Service (toast-manager) options
headline: Service (toast-manager) options
sidebarTitle: Options
sidebarDepth: 0
prev: false
next: false
---

# Service (toast-manager) options

<a name="options">
</a>

## options

Toast manager service options

