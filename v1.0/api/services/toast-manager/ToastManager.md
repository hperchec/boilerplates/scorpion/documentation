---
title: ToastManager
headline: ToastManager
---

# ToastManager

<a name="toastmanager">
</a>

## ToastManager ⇐ <code>VuePluginable</code>

> [context](../../).[services](../)['[toast-manager](./)'].[ToastManager](#)Service **toast-manager**: ToastManager class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [ToastManager](#toastmanager) ⇐ <code>VuePluginable</code>
  - [new ToastManager(options)](#newtoastmanagernew)
  - _instance_
    - [.name](#toastmanager-name)
    - _methods_
      - [.add(toaster)](#toastmanager-add) ⇒ <code>Toaster</code>
      - [.toaster(name)](#toastmanager-toaster) ⇒ <code>Toaster</code>
    - _properties_
      - [.toasters](#toastmanager-toasters) : <code>Array.&lt;Toaster&gt;</code>
  - _static_
    - [.install(_Vue, [options])](#toastmanager-install)

<a name="newtoastmanagernew">
</a>

### new ToastManager(options)

Create a new Toaster Manager

| Param | Type | Description |
| --- | --- | --- |
| options | <code>object</code> | An array of Toaster |
| options.toasters | <code>Array.&lt;Toaster&gt;</code> | An array of Toaster |

<a name="toastmanager-name">
</a>

### toastManager.name

Overrides static VuePluginable properties

<a name="toastmanager-add">
</a>

### toastManager.add(toaster) ⇒ <code>Toaster</code>

Add a Toaster to manager anytime

**Returns**: <code>Toaster</code> - Returns the Toaster

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| toaster | <code>Toaster</code> | The Toaster to add to manager |

<a name="toastmanager-toaster">
</a>

### toastManager.toaster(name) ⇒ <code>Toaster</code>

Use a specific Toaster that was loaded by the manager

**Returns**: <code>Toaster</code> - Returns the Toaster

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The Toaster name to target |

<a name="toastmanager-toasters">
</a>

### toastManager.toasters : <code>Array.&lt;Toaster&gt;</code>

Registered Toasters

**Category**: properties  
**Read only**: true  
<a name="toastmanager-install">
</a>

### ToastManager.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

