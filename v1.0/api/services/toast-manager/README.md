---
title: Service (toast-manager)
headline: Service (toast-manager)
sidebarTitle: toast-manager
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (toast-manager)

<a name="service"></a>

The service "toast-manager" is for UI toast management.It can creates "toasters" and then publish toasts.

## Register

On registering, the service will inject properties to Vue root instance prototype `$app`:- `$app.toast`- `$app.toaster`

## Create

Returns an instance of `ToastManager` created with options.

**Returns**: <code>ToastManager</code> - Once services are created:

```js
Core.service('toast-manager')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['toast-manager']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```jsVue.prototype.$toastManager // => Core.service('toast-manager')```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

**Schema**:

- [$app](#app) : <code>object</code>
  - [.userSettings](#app-usersettings) : <code>object</code>
  - [.toast(toasterName, toast)](#app-toast) ⇒ <code>Toast</code>
  - [.toaster(...args)](#app-toaster) ⇒ <code>Toaster</code>

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

<a name="app-toast">
</a>

###### vm.$app.toast(toasterName, toast) ⇒ <code>Toast</code>

Publish a toast in toasterName

**Returns**: <code>Toast</code> - Returns the Toast instance

| Param | Type | Description |
| --- | --- | --- |
| toasterName | <code>string</code> | Toaster name defined in ToastManager service |
| toast | <code>object</code> | The Toast options |

**Example**

```js
// In any componentthis.$app.toast('Mainframe', {  content: 'Hello world!'}) // => publish toast in  "Mainframe" toaster
```

<a name="app-toaster">
</a>

###### vm.$app.toaster(...args) ⇒ <code>Toaster</code>

Alias for ToastManager.toaster method

**Returns**: <code>Toaster</code> - Returns the Toaster instance

**See**: [ToastManager.toaster](./ToastManager#toastmanager-toaster)

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same parameters as ToastManager.toaster method |

**Example**

```js
// In any componentthis.$app.toaster(...)
```

## Expose (service context)

The service will expose the following properties under `Core.context.services['toast-manager']`:
<a name="expose-toast">
</a>

### .Toast

> Exposed as `Core.context.services['toast-manager'].Toast`

**See**: [Toast](./Toast)

<a name="expose-toaster">
</a>

### .Toaster

> Exposed as `Core.context.services['toast-manager'].Toaster`

**See**: [Toaster](./Toaster)

<a name="expose-toastmanager">
</a>

### .ToastManager

> Exposed as `Core.context.services['toast-manager'].ToastManager`

**See**: [ToastManager](./ToastManager)

