---
title: APIOptions
headline: APIOptions
---

# APIOptions

<a name="apioptions">
</a>

## APIOptions

> [context](../../).[services](../)['[api-manager](./)'].[APIOptions](#)Service **api-manager**: APIOptions class

**Schema**:

- [APIOptions](#apioptions)
  - [new APIOptions(name, driver, options)](#newapioptionsnew)
  - [.name](#apioptions-name) : <code>string</code>
  - [.driver](#apioptions-driver) : <code>string</code>
  - [.options](#apioptions-options) : <code>object</code>

<a name="newapioptionsnew">
</a>

### new APIOptions(name, driver, options)

Create new APIOptions

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The API name |
| driver | <code>string</code> | The library to use in API.supportedDrivers |
| options | <code>object</code> | An object that the driver needs to configure itself |

<a name="apioptions-name">
</a>

### apiOptions.name : <code>string</code>

API name

**Category**: properties  
<a name="apioptions-driver">
</a>

### apiOptions.driver : <code>string</code>

API driver name

**Category**: properties  
<a name="apioptions-options">
</a>

### apiOptions.options : <code>object</code>

Driver options

**Category**: properties  
