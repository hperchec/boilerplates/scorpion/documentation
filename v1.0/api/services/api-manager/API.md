---
title: API
headline: API
---

# API

<a name="api">
</a>

## API

> [context](../../).[services](../)['[api-manager](./)'].[API](#)Service **api-manager**: API class

**Schema**:

- [API](#api)
  - [new API(apiOptions)](#newapinew)
  - _instance_
    - _async methods_
      - [.request(method, url, [options])](#api-request) ⇒ <code>Promise.&lt;\*&gt;</code>
    - _methods_
      - [.setDriver(name, options)](#api-setdriver) ⇒ <code>void</code>
      - [.mapDriverMethods(methods)](#api-mapdrivermethods) ⇒ <code>void</code>
      - [.initAxios(options)](#api-initaxios) ⇒ <code>void</code>
    - _properties_
      - [.name](#api-name) : <code>string</code>
      - [.options](#api-options) : <code>string</code>
      - [.driver](#api-driver) : <code>object</code>
  - _static_
    - _properties_
      - [.supportedDrivers](#api-supporteddrivers) : <code>Array.&lt;string&gt;</code>

<a name="newapinew">
</a>

### new API(apiOptions)

Create a new API

| Param | Type | Description |
| --- | --- | --- |
| apiOptions | <code>APIOptions</code> | An APIOptions object |
| apiOptions.name | <code>string</code> | Name of API |
| apiOptions.driver | <code>string</code> | Driver API |
| apiOptions.options | <code>object</code> | API options |

<a name="api-request">
</a>

### apI.request(method, url, [options]) ⇒ <code>Promise.&lt;\*&gt;</code>

Main request method

**Returns**: <code>Promise.&lt;\*&gt;</code> - The driver Promise response

**Category**: async methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| method | <code>string</code> |  | The method name (ex: GET, POST...) |
| url | <code>string</code> |  | The URL to request, without baseURL (Ex: /users) |
| [options] | <code>object</code> | <code>{}</code> | The specific options for the driver |

<a name="api-setdriver">
</a>

### apI.setDriver(name, options) ⇒ <code>void</code>

Set the driver for the API

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The driver name |
| options | <code>object</code> | The driver options/configuration |

<a name="api-mapdrivermethods">
</a>

### apI.mapDriverMethods(methods) ⇒ <code>void</code>

Map driver methods for requestsFor each HTTP verb, assign driver method:```javascript{  GET: methods.GET, // this.request('GET', <url>, [<options>])  POST: methods.POST, // this.request('POST', <url>, [<options>])  PATCH: methods.PATCH, // this.request('PATCH', <url>, [<options>])  PUT: methods.PUT, // this.request('PUT', <url>, [<options>])  DELETE: methods.DELETE, // this.request('DELETE', <url>, [<options>])  OPTIONS: methods.OPTIONS // this.request('OPTIONS', <url>, [<options>])}```

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| methods | <code>object</code> | A methods object |

<a name="api-initaxios">
</a>

### apI.initAxios(options) ⇒ <code>void</code>

Assign an axios instance to this.driverSee [axios documentation](https://axios-http.com/fr/docs/req_config) to see configuration options

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| options | <code>AxiosRequestConfig</code> | A AxiosRequestConfig object |

<a name="api-name">
</a>

### apI.name : <code>string</code>

API name

**Category**: properties  
<a name="api-options">
</a>

### apI.options : <code>string</code>

API options

**Category**: properties  
<a name="api-driver">
</a>

### apI.driver : <code>object</code>

Get the API driver

**Category**: properties  
**Read only**: true  
<a name="api-supporteddrivers">
</a>

### API.supportedDrivers : <code>Array.&lt;string&gt;</code>

Only 'axios' is supported as a driver for the moment

**Category**: properties  
**Read only**: true  
