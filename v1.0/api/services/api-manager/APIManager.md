---
title: APIManager
headline: APIManager
---

# APIManager

<a name="apimanager">
</a>

## APIManager ⇐ <code>VuePluginable</code>

> [context](../../).[services](../)['[api-manager](./)'].[APIManager](#)Service **api-manager**: APIManager class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [APIManager](#apimanager) ⇐ <code>VuePluginable</code>
  - [new APIManager(options)](#newapimanagernew)
  - _instance_
    - [.reactive](#apimanager-reactive)
    - _methods_
      - [.add(api)](#apimanager-add) ⇒ <code>API</code>
      - [.use(apiName)](#apimanager-use) ⇒ <code>API</code>
    - _properties_
      - [.apis](#apimanager-apis) : <code>Array.&lt;API&gt;</code>
  - _static_
    - [.install(_Vue, [options])](#apimanager-install)

<a name="newapimanagernew">
</a>

### new APIManager(options)

Create a new API Manager

| Param | Type | Description |
| --- | --- | --- |
| options | <code>object</code> | Constructor options |
| options.apis | <code>Array.&lt;API&gt;</code> | An array of API |

<a name="apimanager-reactive">
</a>

### apiManager.reactive

Overrides static VuePluginable properties

<a name="apimanager-add">
</a>

### apiManager.add(api) ⇒ <code>API</code>

Add an API to manager anytime

**Returns**: <code>API</code> - Return this.use(<api.name>)

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| api | <code>API</code> | The API to add to manager |

<a name="apimanager-use">
</a>

### apiManager.use(apiName) ⇒ <code>API</code>

Use a specific API that was loaded by the manager

**Returns**: <code>API</code> - Return the API

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| apiName | <code>string</code> | The API name to target |

<a name="apimanager-apis">
</a>

### apiManager.apis : <code>Array.&lt;API&gt;</code>

Registered APIs

**Category**: properties  
**Read only**: true  
<a name="apimanager-install">
</a>

### APIManager.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

