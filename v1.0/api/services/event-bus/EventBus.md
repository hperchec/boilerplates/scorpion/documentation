---
title: EventBus
headline: EventBus
---

# EventBus

<a name="eventbus">
</a>

## EventBus ⇐ <code>VuePluginable</code>

> [context](../../).[services](../)['[event-bus](./)'].[EventBus](#)Service **event-bus**: EventBus class

**Extends**: <code>VuePluginable</code>  
**Schema**:

- [EventBus](#eventbus) ⇐ <code>VuePluginable</code>
  - [new EventBus()](#neweventbusnew)
  - _instance_
    - [.reactive](#eventbus-reactive)
    - _methods_
      - [.emit(...args)](#eventbus-emit) ⇒ <code>void</code>
      - [.off(...args)](#eventbus-off) ⇒ <code>void</code>
      - [.on(...args)](#eventbus-on) ⇒ <code>void</code>
      - [.once(...args)](#eventbus-once) ⇒ <code>void</code>
    - _properties_
      - [.listeners](#eventbus-listeners) : <code>object</code>
  - _static_
    - [.install(_Vue, [options])](#eventbus-install)

<a name="neweventbusnew">
</a>

### new EventBus()

Create a new instance

<a name="eventbus-reactive">
</a>

### eventBus.reactive

Overrides static VuePluginable properties

<a name="eventbus-emit">
</a>

### eventBus.emit(...args) ⇒ <code>void</code>

emit method: see [Vuejs documentation](https://fr.vuejs.org/v2/api/#vm-emit) for $emit event method

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | same as Vue $emit arguments |

<a name="eventbus-off">
</a>

### eventBus.off(...args) ⇒ <code>void</code>

off method: see [Vuejs documentation](https://fr.vuejs.org/v2/api/#vm-off) for $off event method

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | same as Vue $off arguments |

<a name="eventbus-on">
</a>

### eventBus.on(...args) ⇒ <code>void</code>

on method: see [Vuejs documentation](https://fr.vuejs.org/v2/api/#vm-on) for $on event method

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | same as Vue $on arguments |

<a name="eventbus-once">
</a>

### eventBus.once(...args) ⇒ <code>void</code>

once method: see [Vuejs documentation](https://fr.vuejs.org/v2/api/#vm-once) for $once event method

**Category**: methods  
| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | same as Vue $once arguments |

<a name="eventbus-listeners">
</a>

### eventBus.listeners : <code>object</code>

Returns defined listeners. See also [Vuejs documentation](https://fr.vuejs.org/v2/api/#vm-listeners) for $listeners method

**Category**: properties  
<a name="eventbus-install">
</a>

### EventBus.install(_Vue, [options])

Install method for Vue

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| _Vue | <code>Vue</code> |  | Vue |
| [options] | <code>object</code> | <code>{}</code> | The options of the plugin |

