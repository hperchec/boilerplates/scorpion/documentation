---
title: Service (event-bus)
headline: Service (event-bus)
sidebarTitle: event-bus
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (event-bus)

<a name="service"></a>

The service "event-bus" is a app level event bus.

## Register

On registering, the service will inject properties to Vue root instance prototype `$app`:- `$app.emit`- `$app.off`- `$app.on`- `$app.once`

## Create

Under the ground, service uses an instance of Vue to manage events.Returns an instance of `EventBus` created with options.

**Returns**: <code>EventBus</code> - Once services are created:

```js
Core.service('event-bus')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['event-bus']`.

Plugin will be automatically used by Vue.

The following properties will be set to Vue prototype:```jsVue.prototype.$eventBus // => Core.service('event-bus')```

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

**Schema**:

- [$app](#app) : <code>object</code>
  - [.userSettings](#app-usersettings) : <code>object</code>
  - [.emit(...args)](#app-emit) ⇒ <code>void</code>
  - [.off(...args)](#app-off) ⇒ <code>void</code>
  - [.on(...args)](#app-on) ⇒ <code>void</code>
  - [.once(...args)](#app-once) ⇒ <code>void</code>

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

<a name="app-emit">
</a>

###### vm.$app.emit(...args) ⇒ <code>void</code>

Emit event

**See**: [EventBus.emit](./EventBus#eventbus-emit)

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same parameters as EventBus.emit method |

**Example**

```js
// In any componentthis.$app.emit('my-custom-event')
```

<a name="app-off">
</a>

###### vm.$app.off(...args) ⇒ <code>void</code>

Delete event listener

**See**: [EventBus.off](./EventBus#eventbus-off)

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same parameters as EventBus.off method |

**Example**

```js
// In any componentthis.$app.off('my-custom-event')
```

<a name="app-on">
</a>

###### vm.$app.on(...args) ⇒ <code>void</code>

Create event listener

**See**: [EventBus.on](./EventBus#eventbus-on)

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same parameters as EventBus.on method |

**Example**

```js
// In any componentthis.$app.on('my-custom-event', function () { ... })
```

<a name="app-once">
</a>

###### vm.$app.once(...args) ⇒ <code>void</code>

Create a one-time event listener

**See**: [EventBus.once](./EventBus#eventbus-once)

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same parameters as EventBus.once method |

**Example**

```js
// In any componentthis.$app.once('my-custom-event', function () { ... })
```

## Expose (service context)

The service will expose the following properties under `Core.context.services['event-bus']`:
<a name="expose-eventbus">
</a>

### .EventBus

> Exposed as `Core.context.services['event-bus'].EventBus`

**See**: [EventBus](./EventBus)

