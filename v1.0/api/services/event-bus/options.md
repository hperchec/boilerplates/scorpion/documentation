---
title: Service (event-bus) options
headline: Service (event-bus) options
sidebarTitle: .options
sidebarDepth: 0
prev: false
next: false
---

# Service (event-bus) options

<a name="options">
</a>

## .options

> [context](../).[event-bus](./).[options](#)Default is an empty object

