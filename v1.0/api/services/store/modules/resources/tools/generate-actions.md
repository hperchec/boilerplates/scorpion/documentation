---
title: generate-actions
headline: generate-actions
---

# generate-actions

## Functions

<dl>
<dt><a href="#pushInCollection">pushInCollection(context, item)</a> ⇒ <code>void</code></dt>
<dd><p>Push in collection</p>
</dd>
<dt><a href="#updateCollection">updateCollection(context, payload)</a> ⇒ <code>void</code></dt>
<dd><p>Update collection</p>
</dd>
<dt><a href="#deleteFromCollection">deleteFromCollection(context, key)</a> ⇒ <code>void</code></dt>
<dd><p>Delete from collection</p>
</dd>
<dt><a href="#index">index(context)</a> ⇒ <code>boolean</code> | <code>Error</code></dt>
<dd><p>Index method</p>
</dd>
<dt><a href="#create">create(context, data)</a> ⇒ <code>boolean</code> | <code>Error</code></dt>
<dd><p>Crud: Create method</p>
</dd>
<dt><a href="#retrieve">retrieve(context, id)</a> ⇒ <code>boolean</code> | <code>Error</code></dt>
<dd><p>cRud: Retrieve method</p>
</dd>
<dt><a href="#update">update(context, payload)</a> ⇒ <code>boolean</code> | <code>Error</code></dt>
<dd><p>crUd: Update method</p>
</dd>
<dt><a href="#delete">delete(context, id)</a> ⇒ <code>boolean</code> | <code>Error</code></dt>
<dd><p>cruD: Delete method</p>
</dd>
</dl>

<a name="pushincollection">
</a>

## pushInCollection(context, item) ⇒ <code>void</code>

Push in collection

| Param | Type | Description |
| --- | --- | --- |
| context | <code>object</code> | vuex context |
| item | <code>\*</code> | The item to push |

<a name="updatecollection">
</a>

## updateCollection(context, payload) ⇒ <code>void</code>

Update collection

| Param | Type | Description |
| --- | --- | --- |
| context | <code>object</code> | vuex context |
| payload | <code>object</code> | Method payload |
| payload.key | <code>function</code> \| <code>number</code> \| <code>string</code> | The key to find item to update in collection |
| payload.to | <code>\*</code> | The replacement value |

<a name="deletefromcollection">
</a>

## deleteFromCollection(context, key) ⇒ <code>void</code>

Delete from collection

| Param | Type | Description |
| --- | --- | --- |
| context | <code>object</code> | vuex context |
| key | <code>function</code> \| <code>number</code> \| <code>string</code> | The key to find item to delete from collection |

<a name="index">
</a>

## index(context) ⇒ <code>boolean</code> \| <code>Error</code>

Index method

**Returns**: <code>boolean</code> \| <code>Error</code> - Error or return true

| Param | Type | Description |
| --- | --- | --- |
| context | <code>object</code> | vuex context |

<a name="create">
</a>

## create(context, data) ⇒ <code>boolean</code> \| <code>Error</code>

Crud: Create method

**Returns**: <code>boolean</code> \| <code>Error</code> - Error or return true

| Param | Type | Description |
| --- | --- | --- |
| context | <code>object</code> | vuex context |
| data | <code>object</code> | Request data (See Server API documentation) |

<a name="retrieve">
</a>

## retrieve(context, id) ⇒ <code>boolean</code> \| <code>Error</code>

cRud: Retrieve method

**Returns**: <code>boolean</code> \| <code>Error</code> - Error or return true

| Param | Type | Description |
| --- | --- | --- |
| context | <code>object</code> | vuex context |
| id | <code>number</code> | Resource id |

<a name="update">
</a>

## update(context, payload) ⇒ <code>boolean</code> \| <code>Error</code>

crUd: Update method

**Returns**: <code>boolean</code> \| <code>Error</code> - Error or return true

| Param | Type | Description |
| --- | --- | --- |
| context | <code>object</code> | vuex context |
| payload | <code>object</code> | Method payload |
| payload.id | <code>number</code> | Resource id |
| payload.data | <code>object</code> | Request data (See Server API documentation) |

<a name="delete">
</a>

## delete(context, id) ⇒ <code>boolean</code> \| <code>Error</code>

cruD: Delete method

**Returns**: <code>boolean</code> \| <code>Error</code> - Error or return true

| Param | Type | Description |
| --- | --- | --- |
| context | <code>object</code> | vuex context |
| id | <code>number</code> | Resource id |

