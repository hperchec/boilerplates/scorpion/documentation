---
title: generate-mutations
headline: generate-mutations
---

# generate-mutations

## Functions

<dl>
<dt><a href="#SET_COLLECTION">SET_COLLECTION(state, data)</a> ⇒ <code>void</code></dt>
<dd><p>Mutate state.collection</p>
</dd>
<dt><a href="#PUSH_IN_COLLECTION">PUSH_IN_COLLECTION(state, item)</a> ⇒ <code>void</code></dt>
<dd><p>Mutate state.collection</p>
</dd>
<dt><a href="#UPDATE_COLLECTION">UPDATE_COLLECTION(state, payload)</a> ⇒ <code>void</code></dt>
<dd><p>Mutate state.collection</p>
</dd>
<dt><a href="#DELETE_FROM_COLLECTION">DELETE_FROM_COLLECTION(state, key)</a> ⇒ <code>void</code></dt>
<dd><p>Mutate state.collection</p>
</dd>
<dt><a href="#SET_LOADING_STATUS">SET_LOADING_STATUS(state, payload)</a> ⇒ <code>void</code></dt>
<dd><p>Mutate state.loadingStatus</p>
</dd>
</dl>

<a name="set-collection">
</a>

## SET\_COLLECTION(state, data) ⇒ <code>void</code>

Mutate state.collection

| Param | Type | Description |
| --- | --- | --- |
| state | <code>object</code> | vuex store state |
| data | <code>Array.&lt;object&gt;</code> | Data received from server |

<a name="pushincollection">
</a>

## PUSH\_IN\_COLLECTION(state, item) ⇒ <code>void</code>

Mutate state.collection

| Param | Type | Description |
| --- | --- | --- |
| state | <code>object</code> | vuex store state |
| item | <code>\*</code> | The item to push |

<a name="update-collection">
</a>

## UPDATE\_COLLECTION(state, payload) ⇒ <code>void</code>

Mutate state.collection

| Param | Type | Description |
| --- | --- | --- |
| state | <code>object</code> | vuex store state |
| payload | <code>object</code> | Mutation payload |
| payload.key | <code>function</code> \| <code>number</code> \| <code>string</code> | The key to find item to update in collection |
| payload.to | <code>\*</code> | The replacement value |

<a name="deletefromcollection">
</a>

## DELETE\_FROM\_COLLECTION(state, key) ⇒ <code>void</code>

Mutate state.collection

| Param | Type | Description |
| --- | --- | --- |
| state | <code>object</code> | vuex store state |
| key | <code>function</code> \| <code>number</code> \| <code>string</code> | The key to find item to delete in collection |

<a name="setloadingstatus">
</a>

## SET\_LOADING\_STATUS(state, payload) ⇒ <code>void</code>

Mutate state.loadingStatus

| Param | Type | Description |
| --- | --- | --- |
| state | <code>object</code> | vuex store state |
| payload | <code>object</code> | Mutation payload |
| payload.name | <code>string</code> | Loading name |
| payload.status | <code>boolean</code> | Loading status (true|false) |

