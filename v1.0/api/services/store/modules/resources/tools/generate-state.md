---
title: generate-state
headline: generate-state
---

# generate-state

## Members

<dl>
<dt><a href="#collection">collection</a> : <code>Collection</code></dt>
<dd><p>Resource collection</p>
</dd>
<dt><a href="#loadingStatus">loadingStatus</a> : <code>object</code></dt>
<dd><p>Save the loading status</p>
</dd>
</dl>

<a name="collection">
</a>

## collection : <code>Collection</code>

Resource collection

<a name="loadingstatus">
</a>

## loadingStatus : <code>object</code>

Save the loading status

