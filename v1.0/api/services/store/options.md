---
title: Service (store) options
headline: Service (store) options
sidebarTitle: .options
sidebarDepth: 0
prev: false
next: false
---

# Service (store) options

<a name="options">
</a>

## .options

> [context](../).[store](./).[options](#)Accepts all [Vuex.Store](https://v3.vuex.vuejs.org/api/#vuex-store-constructor-options) constructor options plus the following:- `logger`- `log`- `translator`- `translate`- `localStorageManager`By default, the `modules` option is an empty Object. The modules defined in `Core.context.services['store'].modules` like `System` and `Resources`will be auto-injected at service creation.The `plugins` option is an empty array. At service creation, the `plugins` option will be a concatenated array of pluginsdefined in `Core.context.services['store'].plugins` and the `plugins` option array content.::: tip SEE ALSO[Vuex.Store constructor options documentation](https://v3.vuex.vuejs.org/api/#vuex-store-constructor-options):::

**Schema**:

- [.options](#options)
  - [.logger()](#options-logger) ⇒ <code>\*</code>
  - [.log(...args)](#options-log) ⇒ <code>\*</code>
  - [.translator()](#options-translator) ⇒ <code>\*</code>
  - [.translate(...args)](#options-translate) ⇒ <code>\*</code>
  - [.localStorageManager()](#options-localstoragemanager) : <code>\*</code>

<a name="options-logger">
</a>

### options.logger() ⇒ <code>\*</code>

Default is 'logger' service

**Returns**: <code>\*</code> - Logger instance/object

<a name="options-log">
</a>

### options.log(...args) ⇒ <code>\*</code>

Function to log in console. Default is 'logger' service 'consoleLog' method

**Returns**: <code>\*</code> - Default returns nothing

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same arguments as service method |

<a name="options-translator">
</a>

### options.translator() ⇒ <code>\*</code>

Translator. Default is 'i18n' service

**Returns**: <code>\*</code> - Translator instance/object

<a name="options-translate">
</a>

### options.translate(...args) ⇒ <code>\*</code>

Function to translate string. Default is 'i18n' service 't' method

**Returns**: <code>\*</code> - Default returns string

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Same arguments as service method |

<a name="options-localstoragemanager">
</a>

### options.localStorageManager() : <code>\*</code>

Local storage interface. Default is 'local-storage-manager' service

