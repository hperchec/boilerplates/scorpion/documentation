---
title: Service (store)
headline: Service (store)
sidebarTitle: store
initialOpenGroupIndex: -1
prev: ../
next: ./options
---

# Service (store)

<a name="service"></a>

The service "store" is for data state and sharing between components.It uses the [vuex](https://vuex.vuejs.org/) package.

## Register

On registering, if the service "logger" is registered too, the service will define a `Logger` [type](../logger/Logger#types) as following:```jsstore: {  badgeContent: '[STORE]',  badgeColor: '#BDC6FB',  badgeBgColor: '#5168F5',  messageColor: '#091D9A',  prependMessage: '☁'}```

## Create

Returns an instance of `Vuex.Store` created with Vuex options.

**Returns**: <code>Vuex.Store</code> - Once services are created:

```js
Core.service('store')
```

## Vue

### Plugin

On registering, service Vue plugin will be set in `Core.context.vue.plugins['store']`.

Plugin will be automatically used by Vue.

::: tip SEE ALSO[vuex](https://vuex.vuejs.org/) documentation:::

### Root instance

<a name="app"></a>

#### Prototype $app property

On registering, the service will inject properties to \`Core.context.vue.root.prototype.app\`.

> Properties will be shared by all components under \`vm.$app\` where \`vm\` is the component instance.

<a name="app-usersettings"></a>

##### User settings

Service will define the following user settings keys:

##### Others

## Expose (service context)

The service will expose the following properties under `Core.context.services['store']`:
<a name="expose-modules">
</a>

### .modules

> Exposed as `Core.context.services['store'].modules`

**See**: [modules](./modules)

<a name="expose-plugins">
</a>

### .plugins

> Exposed as `Core.context.services['store'].plugins`

**See**: [plugins](./plugins)

