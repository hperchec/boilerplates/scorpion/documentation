---
title: safe
headline: safe
sidebarTitle: safe
---

# safe

<a name="safe">
</a>

## safe ⇒ <code>\*</code>

The sync version of [safeAsync](./safe-async).Encapsulate in a try/catch block a function to execute

**Returns**: <code>\*</code> - Returns the value returned by `func`, else Error

| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> | The function to execute |

**Example**

```js
safe(function () {  // Some actions that can throw errors/exceptions})
```

