---
title: sortBy
headline: sortBy
sidebarTitle: sortBy
---

# sortBy

<a name="sortby">
</a>

## sortBy ⇒ <code>Array</code>

Sort array of objects (lodash.sortby).::: tip See also[lodash.union](https://www.npmjs.com/package/lodash.union) package documentation:::

**Returns**: <code>Array</code> - Returns the sorted array

| Param | Type | Description |
| --- | --- | --- |
| array | <code>Array</code> | The array |
| sortFct | <code>function</code> | The sort function |

**Example**

```js
const array = [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]sortBy(array, function (item) { return item.id }) // Return -> [ { id: 1, name: 'John' }, { id: 2, name: 'Jane' } ]sortBy(array, function (item) { return item.name }) // Return -> [ { id: 2, name: 'Jane' }, { id: 1, name: 'John' } ]
```

