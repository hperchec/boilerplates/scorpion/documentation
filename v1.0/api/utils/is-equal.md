---
title: isEqual
headline: isEqual
sidebarTitle: isEqual
---

# isEqual

<a name="isequal">
</a>

## isEqual ⇒ <code>boolean</code>

Compare two values (lodash.isequal).::: tip See also[lodash.isequal](https://www.npmjs.com/package/lodash.isequal) package documentation:::

**Returns**: <code>boolean</code> - Returns lodash.isequal result

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to compare |
| other | <code>\*</code> | The other value to compare |

**Example**

```js
isEqual(1, 2) // Return -> falseisEqual([ 'toto', 'tata' ], [ 'tata', 'toto' ]) // Return -> true
```

