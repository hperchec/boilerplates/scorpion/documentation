---
title: toRawType
headline: toRawType
sidebarTitle: toRawType
---

# toRawType

<a name="torawtype">
</a>

## toRawType ⇒ <code>string</code>

Get the raw type string of a value, e.g., [object Object].

**Returns**: <code>string</code> - The raw type string

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The variable to check |

**Example**

```js
toRawType(new Date()) // => 'Date'toRawType({ a: 1, b: 2 }) // => 'Object'toRawType('Hello') // => 'String'
```

