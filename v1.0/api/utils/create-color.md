---
title: createColor
headline: createColor
sidebarTitle: createColor
---

# createColor

<a name="createcolor">
</a>

## createColor ⇒ <code>Color</code>

Create a color::: tip See also[color](https://www.npmjs.com/package/color) package documentation:::

**Returns**: <code>Color</code> - An instance of Color

**Example**

```js
const color = createColor('#FFFFFF')const color = createColor('rgb(255, 255, 255)')const color = createColor({r: 255, g: 255, b: 255})const color = createColor.rgb(255, 255, 255)const color = createColor.rgb([255, 255, 255])
```

