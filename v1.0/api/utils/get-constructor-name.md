---
title: getConstructorName
headline: getConstructorName
sidebarTitle: getConstructorName
---

# getConstructorName

<a name="getconstructorname">
</a>

## getConstructorName ⇒ <code>string</code>

Use function string name to check built-in types,because a simple equality check will fail

**Returns**: <code>string</code> - Returns the constructor name

| Param | Type | Description |
| --- | --- | --- |
| fn | <code>function</code> | The constructor |

**Example**

```js
getConstructorName(Date) // => 'Date'
```

