---
title: snakecase
headline: snakecase
sidebarTitle: snakecase
---

# snakecase

<a name="snakecase">
</a>

## snakecase ⇒ <code>string</code>

Convert a string to snakecase *([Defined as Vue filter](../context/vue/filters#filters-snakecase))*.::: tip See also[lodash.snakecase](https://www.npmjs.com/package/lodash.snakecase) package documentation:::

**Returns**: <code>string</code> - Returns the "snakecased" string

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string to snakecase |

**Example**

```js
snakecase('Foo Bar') // => 'foo_bar'
```

