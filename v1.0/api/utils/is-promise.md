---
title: isPromise
headline: isPromise
sidebarTitle: isPromise
---

# isPromise

<a name="ispromise">
</a>

## isPromise ⇒ <code>boolean</code>

Check if value is Promise

**Returns**: <code>boolean</code> - Returns true if value is a Promise

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to check |

**Example**

```js
isPromise(new Promise(...)) // => trueisPromise(new Date()) // => false
```

