---
title: completeAssign
headline: completeAssign
sidebarTitle: completeAssign
---

# completeAssign

<a name="completeassign">
</a>

## completeAssign ⇒ <code>object</code>

This is an assign function that copies full descriptors.See also https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Object/assign#copier_des_accesseurs

**Returns**: <code>object</code> - Returns mutated target

| Param | Type | Description |
| --- | --- | --- |
| target | <code>object</code> | The target object |
| ...sources | <code>object</code> | The sources |

**Example**

```js
completeAssign({}, { 'a': 1, get b () { return 'b' }, set b (value) { ... } }) // Keep property descriptors (accessors/mutators, enumerable, configurable...)
```

