---
title: isObject
headline: isObject
sidebarTitle: isObject
---

# isObject

<a name="isobject">
</a>

## isObject ⇒ <code>boolean</code>

Quick object check - this is primarily used to tellObjects from primitive values when we know the valueis a JSON-compliant type.

**Returns**: <code>boolean</code> - Returns true if value is object type

| Param | Type | Description |
| --- | --- | --- |
| obj | <code>\*</code> | The value to check |

**Example**

```js
isObject({ a: 1, b: 2 }) // => trueisObject(new Date()) // => trueisObject('Hello') // => false
```

