---
title: getPrototypeChain
headline: getPrototypeChain
sidebarTitle: getPrototypeChain
---

# getPrototypeChain

<a name="getprototypechain">
</a>

## getPrototypeChain ⇒ <code>Array.&lt;object&gt;</code>

Returns an array containing the prototype chain of `value`

**Returns**: <code>Array.&lt;object&gt;</code> - Returns the prototype chain

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value |

**Example**

```js
getPrototypeChain({ foo: 'bar' }) // => [ Object ]
```

