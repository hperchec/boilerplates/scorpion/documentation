---
title: dateFormat
headline: dateFormat
sidebarTitle: dateFormat
---

# dateFormat

<a name="dateformat">
</a>

## dateFormat ⇒ <code>string</code>

Format a date *([Defined as Vue filter](../context/vue/filters#filters-dateformat))*.::: tip See also[dateformat](https://www.npmjs.com/package/dateformat) package documentation:::

**Returns**: <code>string</code> - Returns the formatted date as string

| Param | Type | Description |
| --- | --- | --- |
| date | <code>Date</code> | The date to format |
| format | <code>string</code> | The target format |

**Example**

```js
dateFormat(new Date(), 'yyyy-mm-dd') // => '1900-01-01'
```

