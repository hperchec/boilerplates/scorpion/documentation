---
title: isPrimitive
headline: isPrimitive
sidebarTitle: isPrimitive
---

# isPrimitive

<a name="isprimitive">
</a>

## isPrimitive ⇒ <code>boolean</code>

Check if value is primitive.

**Returns**: <code>boolean</code> - Returns true if value is primitive type

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The variable to check |

**Example**

```js
isPrimitive('Hello') // => trueisPrimitive(new Date()) // => false
```

