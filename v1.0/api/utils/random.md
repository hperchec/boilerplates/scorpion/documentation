---
title: random
headline: random
sidebarTitle: random
---

# random

<a name="random">
</a>

## random : <code>object</code>

<a name="random-alphanumeric">
</a>

### random.alphaNumeric ⇒ <code>string</code>

Generate random alphanumeric string based on lodash.samplesize function.::: tip See also[lodash.samplesize](https://www.npmjs.com/package/lodash.samplesize) package documentation:::

**Returns**: <code>string</code> - Returns the created random string

| Param | Type | Description |
| --- | --- | --- |
| length | <code>number</code> | The length of returned random string |
| [charset] | <code>Array</code> \| <code>string</code> | An array or string of characters |

**Example**

```js
alphaNumeric(5) // Return -> 'Gy85f' (example)alphaNumeric(5, 'ABCDE') // Return -> 'AADBE' (example)
```

