---
title: cloneDeep
headline: cloneDeep
sidebarTitle: cloneDeep
---

# cloneDeep

<a name="clonedeep">
</a>

## cloneDeep ⇒ <code>\*</code>

This method is like 'clone' except that it recursively clones value.::: tip See also[lodash.cloneDeep](https://lodash.com/docs/4.17.15#cloneDeep) package documentation:::

**Returns**: <code>\*</code> - Returns the depp cloned object

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to clone |

**Example**

```js
cloneDeep({ 'a': 1, 'b': { foo: 'bar' } }) // creates a deep clone
```

