---
title: isValidHttpUrl
headline: isValidHttpUrl
sidebarTitle: isValidHttpUrl
---

# isValidHttpUrl

<a name="isvalidhttpurl">
</a>

## isValidHttpUrl ⇒ <code>URL</code> \| <code>boolean</code>

Check if `str` is a valid http URL

**Returns**: <code>URL</code> \| <code>boolean</code> - Return new URL object if valid, else false

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The url string |

**Example**

```js
isValidHttpUrl('http://example.com/') // success: return new URL('http://example.com/')isValidHttpUrl('example.com') // fail
```

