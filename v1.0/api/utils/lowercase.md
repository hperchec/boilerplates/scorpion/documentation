---
title: lowercase
headline: lowercase
sidebarTitle: lowercase
---

# lowercase

<a name="lowercase">
</a>

## lowercase ⇒ <code>string</code>

Convert a string to lowercase *([Defined as Vue filter](../context/vue/filters#filters-lowercase))*.::: tip See also[lodash.lowercase](https://www.npmjs.com/package/lodash.lowercase) package documentation:::

**Returns**: <code>string</code> - Returns the "lowercased" string

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string to lowercase |

**Example**

```js
lowercase('Foo BAR') // => 'foo bar'
```

