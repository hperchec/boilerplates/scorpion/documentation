---
title: debounce
headline: debounce
sidebarTitle: debounce
---

# debounce

<a name="debounce">
</a>

## debounce ⇒ <code>void</code>

Debounce callback (lodash.debounce).::: tip See also[lodash.debounce](https://www.npmjs.com/package/lodash.debounce) package documentation:::

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| func | <code>function</code> |  | The callback function |
| [wait] | <code>number</code> | <code>0</code> | Time delay |
| [options] | <code>object</code> | <code>{}</code> | Options |

**Example**

```js
debounce(myFunc, 150) // call 'myfunc()' after 150 ms
```

