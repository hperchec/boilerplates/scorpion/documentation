---
title: isDef
headline: isDef
sidebarTitle: isDef
---

# isDef

<a name="isdef">
</a>

## isDef ⇒ <code>boolean</code>

Check if value is defined (!== undefined)

**Returns**: <code>boolean</code> - Returns true if value is defined (!== undefined)

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The variable to check |

**Example**

```js
isDef(value) // => true if value is defined
```

