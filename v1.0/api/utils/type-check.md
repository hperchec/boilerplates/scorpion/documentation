---
title: typeCheck
headline: typeCheck
sidebarTitle: typeCheck
---

# typeCheck

<a name="typecheck">
</a>

## typeCheck ⇒ <code>boolean</code>

Based on Vue.js prop type parser, check type of a variable

**Returns**: <code>boolean</code> - Returns true if type match

| Param | Type | Description |
| --- | --- | --- |
| type | <code>\*</code> | The type to check (as a Vue prop type format) |
| value | <code>\*</code> | The value to check |

**Example**

```js
typeCheck(String, 'John Doe') // truetypeCheck(MyCustomClass, 1) // falsetypeCheck([String, Number], '1') // true
```

