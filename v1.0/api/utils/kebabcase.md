---
title: kebabcase
headline: kebabcase
sidebarTitle: kebabcase
---

# kebabcase

<a name="kebabcase">
</a>

## kebabcase ⇒ <code>string</code>

Convert a string to kebabcase *([Defined as Vue filter](../context/vue/filters#filters-kebabcase))*.::: tip See also[lodash.kebabcase](https://www.npmjs.com/package/lodash.kebabcase) package documentation:::

**Returns**: <code>string</code> - Returns the "kebabcased" string

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string to kebabcase |

**Example**

```js
kebabcase('Foo Bar') // => 'foo-bar'
```

