---
title: camelcase
headline: camelcase
sidebarTitle: camelcase
---

# camelcase

<a name="camelcase">
</a>

## camelcase ⇒ <code>string</code>

Convert a string to camelcase *([Defined as Vue filter](../context/vue/filters#filters-camelcase))*.::: tip See also[lodash.camelcase](https://www.npmjs.com/package/lodash.camelcase) package documentation:::

**Returns**: <code>string</code> - Returns the "camelcased" string

| Param | Type | Description |
| --- | --- | --- |
| str | <code>string</code> | The string to camelcase |

**Example**

```js
camelcase('Foo Bar') // => 'fooBar'
```

