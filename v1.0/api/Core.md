---
title: Core class
headline: Core class
sidebarTitle: Core
sidebarDepth: 0
prev: ./utils/
next: ./Service
---

# Core class

<a name="core">
</a>

## Core

Scorpion UI Core class

**Schema**:

- [Core](#core)
  - _static async methods_
    - [.setup()](#core-setup) ⇒ <code>Promise.&lt;void&gt;</code>
    - [.runApp()](#core-runapp) ⇒ <code>Promise.&lt;void&gt;</code>
  - _static methods_
    - [.init(_Vue)](#core-init) ⇒ <code>void</code>
    - [.registerService(service)](#core-registerservice) ⇒ <code>void</code>
    - [.registerVuePlugin(name, plugin, [options])](#core-registervueplugin) ⇒ <code>void</code>
    - [.configure(config)](#core-configure) ⇒ <code>void</code>
    - [.resolveConfig([config])](#core-resolveconfig) ⇒ <code>object</code>
    - [.getPluginableServices()](#core-getpluginableservices) ⇒ <code>object</code>
    - [.service(identifier)](#core-service) ⇒ <code>void</code>
    - [.onBeforeUseVuePlugin(name, func)](#core-onbeforeusevueplugin) ⇒ <code>void</code>
    - [.onBeforeCreateService(identifier, func)](#core-onbeforecreateservice) ⇒ <code>void</code>
    - [.onServiceCreated(identifier, func)](#core-onservicecreated) ⇒ <code>void</code>
    - [.onBeforeAppBoot(func)](#core-onbeforeappboot) ⇒ <code>void</code>
    - [.onAppBoot(func)](#core-onappboot) ⇒ <code>void</code>
  - _static properties_
    - [.version](#core-version) : <code>string</code>
    - [.isInitialized](#core-isinitialized) : <code>boolean</code>
    - [.isSetup](#core-issetup) : <code>boolean</code>
    - [.Vue](#core-vue) : <code>function</code>
    - [.config](#core-config) : <code>object</code>
    - [.context](#core-context) : <code>object</code>

<a name="core-setup">
</a>

### Core.setup() ⇒ <code>Promise.&lt;void&gt;</code>

Setup Core. It will:- create and use Vue plugins- create servicesCore must be *initialized* before setup.

**Category**: static async methods  
<a name="core-runapp">
</a>

### Core.runApp() ⇒ <code>Promise.&lt;void&gt;</code>

Run the app. It will:- call "BEFORE_APP_BOOT" hook- call "APP_BOOT" hook- create new Vue instance with `Core.context.vue.root.options`All options in `Core.context.vue.root.options` are injected.Pluginable services that have `config.rootOption` defined are auto-injected too.(example: `router` service is passed as `{ router: Core.service('router') }`)New Vue instance will be assigned to `window.<propertyName>` where `propertyName` is the value of `Core.config.vue.root.name`.Core must be *initialized* and *setup* before running app.

**Category**: static async methods  
<a name="core-init">
</a>

### Core.init(_Vue) ⇒ <code>void</code>

Initialize Core. Takes `Vue` as first argument.It will set `Core.Vue` and register services.

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| _Vue | <code>function</code> | Vue |

<a name="core-registerservice">
</a>

### Core.registerService(service) ⇒ <code>void</code>

Register a service. Takes a service object as first argument. See services documentation.Core must be *initialized* before registering service.

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| service | <code>Service</code> | The service object |

<a name="core-registervueplugin">
</a>

### Core.registerVuePlugin(name, plugin, [options]) ⇒ <code>void</code>

Register a Vue plugin.Core must be *initialized* before registering Vue plugin.

**Category**: static methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| name | <code>string</code> |  | The plugin name |
| plugin | <code>object</code> |  | The plugin |
| [options] | <code>object</code> | <code>{}</code> | The plugin options |

<a name="core-configure">
</a>

### Core.configure(config) ⇒ <code>void</code>

Configure Core. Takes an object that follows [global configuration](./config) schema as first argument.The given object will be [deep merged](./utils/deep-merge) with Core configuration. The result will be set in `Core.config`.Core must be *initialized* before configuring.

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| config | <code>object</code> | The config |

<a name="core-resolveconfig">
</a>

### Core.resolveConfig([config]) ⇒ <code>object</code>

Resolve configuration. Returns the result of `config` parameter object and the `Core.config` object deep merge.

**Returns**: <code>object</code> - Returns the resolved config

**Category**: static methods  
| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [config] | <code>object</code> | <code>{}</code> | (Optional) The config object |

<a name="core-getpluginableservices">
</a>

### Core.getPluginableServices() ⇒ <code>object</code>

Returns registered services that must be treated as Vue plugins

**Returns**: <code>object</code> - Returns an object of "pluginable" services

**Category**: static methods  
<a name="core-service">
</a>

### Core.service(identifier) ⇒ <code>void</code>

Access created service

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| identifier | <code>string</code> | The service identifier |

<a name="core-onbeforeusevueplugin">
</a>

### Core.onBeforeUseVuePlugin(name, func) ⇒ <code>void</code>

**BEFORE_USE_VUE_PLUGIN** hook append method.The function to append, which can be *async* takes two arguments:```tsfunction (plugin: Object, options: any) => void | Promise<void>```- **plugin**: `{object}` the Vue plugin object- **options**: `{any}` the plugin options

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The plugin name |
| func | <code>function</code> | The function to append to the hook queue |

**Example**

```js
Core.onBeforeUseVuePlugin('<name>', (plugin, options) => {  // Do logic before use Vue plugin...})
```

<a name="core-onbeforecreateservice">
</a>

### Core.onBeforeCreateService(identifier, func) ⇒ <code>void</code>

**BEFORE_CREATE_SERVICE** hook append method.The function to append has the following signature:```tsfunction () => void | Promise<void>```

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| identifier | <code>string</code> | The service unique identifier |
| func | <code>function</code> | The function to append to the hook queue |

**Example**

```js
Core.onBeforeCreateService('<identifier>', () => {  // Do logic before create service...})
```

<a name="core-onservicecreated">
</a>

### Core.onServiceCreated(identifier, func) ⇒ <code>void</code>

**SERVICE_CREATED** hook append method.The function to append, that can be async, takes one argument:```tsfunction () => void | Promise<void>```- **service**: `{*}` the created service

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| identifier | <code>string</code> | The service unique identifier |
| func | <code>function</code> | The function to append to the hook queue |

**Example**

```js
Core.onServiceCreated('<identifier>', (service) => {  // Service is created now. Do logic...})
```

<a name="core-onbeforeappboot">
</a>

### Core.onBeforeAppBoot(func) ⇒ <code>void</code>

**BEFORE_APP_BOOT** hook append method.The function to append can be *async*:```tsfunction () => void | Promise<void>```

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> | The function to append to the hook queue |

**Example**

```js
Core.onBeforeAppBoot(() => {  // Services are created and accessible in this hook  Core.service('logger').consoleLog('Before app boot!')})
```

<a name="core-onappboot">
</a>

### Core.onAppBoot(func) ⇒ <code>void</code>

**APP_BOOT** hook append method.The function to append can be *async*:```tsfunction () => void | Promise<void>```

**Category**: static methods  
| Param | Type | Description |
| --- | --- | --- |
| func | <code>function</code> | The function to append to the hook queue |

**Example**

```js
Core.onAppBoot(() => {  // Services are created and accessible in this hook  Core.service('logger').consoleLog('App boot!')})
```

<a name="core-version">
</a>

### Core.version : <code>string</code>

Core version

**Category**: static properties  
**Read only**: true  
<a name="core-isinitialized">
</a>

### Core.isInitialized : <code>boolean</code>

Returns true if Core is initialized

**Category**: static properties  
**Read only**: true  
<a name="core-issetup">
</a>

### Core.isSetup : <code>boolean</code>

Returns true if Core is setup

**Category**: static properties  
**Read only**: true  
<a name="core-vue">
</a>

### Core.Vue : <code>function</code>

Vue class reference. Will be defined once `init()` is called

**Default**: <code>undefined</code>  
**Category**: static properties  
**Read only**: true  
<a name="core-config">
</a>

### Core.config : <code>object</code>

Contains default configuration.Will be overwritten once configure() is called.

**Category**: static properties  
<a name="core-context">
</a>

### Core.context : <code>object</code>

The app context. See also [context](./context) documentation.

**Category**: static properties  
